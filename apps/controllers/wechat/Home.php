<?php 
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
/**
 * @author chaituan@126.com
 */
use EasyWeChat\Foundation\Application;
class Home extends WechatCommon {
	
	function issue(){
		if(is_ajax_request()){
			$data = Posts();
			$this->load->model(array('admin/Tie_model'=>'do'));
			$add_data = array(
					'lid'=>1,
					'uid'=>$this->User['id'],
					'nickname'=>$this->User['nicknames'],
					'header'=>$this->User['thumb'],
					'cid'=>$data['cid'],
					'cname'=>$data['cname'],
					'content'=>$data['content'],
					'thumb'=>$data['thumb'],
					'mobile'=>$data['mobile'],
					'address_name'=>$data['address_name'],
					'latitude'=>$data['latitude'],
					'longitude'	=>$data['longitude'],
					'addtime'=>time()
			);
			$result = $this->do->add($add_data);
			is_AjaxResult($result);
		}
	}
	
	function shop(){
		if(is_ajax_request()){
			$data = Posts();
			$this->load->model(array('admin/Shop_model'=>'do'));
			$sb = array('wifi'=>0,'stopc'=>0,'alipay'=>0,'wxpay'=>0);
			$add_data = array(
					'uid'=>$this->User['id'],
					'sname'=>$data['sname'],
					'mobile'=>$data['mobile'],
					'header'=>$data['header'],
					'thumb'=>$data['thumb'],
					'scid'=>$data['scid'],
					'scname'=>$data['scname'],
					'cid'=>$data['cid'],
					'cname'=>$data['cname'],
					'content'=>$data['content'],
					'star_time'=>$data['star_time'],
					'address'=>$data['address'],
					'latitude'=>$data['latitude'],
					'longitude'	=>$data['longitude'],
					'addtime'=>time()
			);
			if($data['sb']){
				$sb_post = explode(',', $data['sb']);
				foreach ($sb as $k=>$v){
					if(in_array($k, $sb_post)){
						$add_data[$k] = 1;
					}else{
						$add_data[$k] = 0;
					}
				}
			}
			if($data['query']=='edit'){
				$thumb = explode(',', $add_data['thumb']);
				if($thumb){
					foreach ($thumb as $v){
						$news[] = str_replace(HTTP_HOST, '', $v);
					}
				}
				$add_data['thumb'] = implode(',', $news);
				$result = $this->do->updates($add_data,array('id'=>$data['id']));
				if($result){
					$add_data['id'] = $data['id'];
					$add_data = $this->chuli_shuju($add_data);
					AjaxResult(1,'',$add_data);
				}else{
					AjaxResult_error();
				}
			}else{
				$result = $this->do->add($add_data);
				if($result){
					$add_data['id'] = $result;
					$add_data = $this->chuli_shuju($add_data);
					AjaxResult(1,'',$add_data);
				}else{
					AjaxResult_error();
				}
			}
		}
	}
	
	private function chuli_shuju($item){//处理店铺返回到页面的数据
		$item['header'] = base_url($item['header']);
		$item['thumb'] = addimg_url(explode(',', $item['thumb']));
		$item['shebei'] = array(
				array('name'=>'wifi','value'=>'WIFI','checked'=>$item['wifi']?true:false),
				array('name'=>'stopc','value'=>'停车位','checked'=>$item['stopc']?true:false),
				array('name'=>'alipay','value'=>'支付宝付款','checked'=>$item['alipay']?true:false),
				array('name'=>'wxpay','value'=>'微信付款','checked'=>$item['wxpay']?true:false),
		);
		return $item;
	}
	
	function card_hb(){//卡片海报
		if(is_ajax_request()){
			$id = Posts('id','num');
			$qrcode_url = '/res/upload/card/qr_'.$id.'.jpg';//用户二维码
			$qrcode = $this->downImage($qrcode_url,$id,"pages/card/top/detail");
			AjaxResult(1,'',base_url($qrcode));
		}
	}
	
	private function downImage($img,$scene,$url){
		if(!is_file(substr($img,1))){
			$app = new Application(array('mini_program'=>array('app_id'=>WX_APPID,'secret'=> WX_APPSecret)));
			$result = $app->mini_program->qrcode->getAppCodeUnlimit($scene,$url);
// 			$result = $app->mini_program->qrcode->getAppCode($url);
			$filename = FCPATH.$img;
			file_put_contents($filename, $result);
		}
		return $img;
	}
}
