<?php 
/**
 * chaituan@126.com
 */
class Image{
    /**
     * 
     * @param  $uname 图片名字 不带后缀
     * @param  $srcImg 背景图
     * @param  $waterImg 水印图
     * @param  $savepath 保存路径
     * @param  $savename 保存图片名字带后缀
     * @param  $l   left
     * @param  $t   top
     * @param  $width  宽
     * @param  $height 高
     * @param  $alpha 色度
     * @return string
     */
	function img_water_mark($src,$data,$save, $alpha=100){
		extract($data);
		if($w==0||$h==0)return $src;
		$savefile = $save;
// 		$this->is_path($savepath);
		$srcinfo = @getimagesize($srcImg);
		if (!$srcinfo) {
			return -1; //原文件不存在
		}
		$filename = "weter.jpg";
		if(!$this->isLocalImage($waterImg)){
			$this->downImage($waterImg, $savepath.$uname.'.jpg');
			$waterImg = $savepath.$uname.'.jpg';
			$waterImgs = $savepath.$uname.'.jpg';
		}
		$this->resizejpg($waterImg,$filename,$width,$height);
			
		
		$waterinfo = @getimagesize($filename);
		if (!$waterinfo)return -2; //水印图片不存在
		
		$srcImgObj = $this->image_create_from_ext($srcImg);
		if (!$srcImgObj)return -3; //原文件图像对象建立失败
		
		$waterImgObj = $this->image_create_from_ext($filename);
		if (!$waterImgObj)return -4; //水印文件图像对象建立失败
		
		$p_x = $x;
		$p_y = $y;
		imagecopymerge($srcImgObj, $waterImgObj, $p_x, $p_y, 0, 0, $waterinfo[0], $waterinfo[1], $alpha);
		switch ($srcinfo[2]) {
			case 1: imagegif($srcImgObj, $savefile); break;
			case 2: imagejpeg($srcImgObj, $savefile); break;
			case 3: imagepng($srcImgObj, $savefile); break;
			default: return -5; //保存失败
		}
		imagedestroy($srcImgObj);
		imagedestroy($waterImgObj);
		unlink($filename);
		// 	    unlink($waterImgs);
		return $savefile;
	}
	
	function img_water_marks($src,$waterdata,$save,$alpha=0){
		$srcImgObj = $this->image_create_from_ext($src);
		foreach ($waterdata as $k=>$v){
			$filename = "weter$k".".jpg";
			if(!$this->isLocalImage($v['water']['file'])){
				$this->downImage($v['water']['file'], $v['water']['urlname']);
				$this->resizejpg($v['water']['urlname'],$filename,$v['pores']['w'],$v['pores']['h'],$v['pores']['w']);
			}else{
				$this->resizejpg($v['water']['file'],$filename,$v['pores']['w'],$v['pores']['h']);
			}
			$waterinfo = @getimagesize($filename);
			$waterImgObjarr[] = $waterImgObj = $this->image_create_from_ext($filename);
			$this->imagecopymerge_alpha($srcImgObj, $waterImgObj, $v['pores']['x'], $v['pores']['y'], 0, 0, $waterinfo[0], $waterinfo[1], $alpha);
		}
		
		imagejpeg($srcImgObj, $save,95);
		foreach ($waterImgObjarr as $v){
			imagedestroy($v);
		}
		return $save;
	}
	
	function imagecopymerge_alpha($dst_im, $src_im, $dst_x, $dst_y, $src_x, $src_y, $src_w, $src_h, $pct){
		$opacity = $pct;
		// getting the watermark width
		$w = imagesx($src_im);
		// getting the watermark height
		$h = imagesy($src_im);
		
		// creating a cut resource
		$cut = imagecreatetruecolor($src_w, $src_h);
		// copying that section of the background to the cut
		imagecopy($cut, $dst_im, 0, 0, $dst_x, $dst_y, $src_w, $src_h);
		// inverting the opacity
		$opacity = 100 - $opacity;
	
		// placing the watermark now
		imagecopy($cut, $src_im, 0, 0, $src_x, $src_y, $src_w, $src_h);
		imagecopymerge($dst_im, $cut, $dst_x, $dst_y, $src_x, $src_y, $src_w, $src_h, $opacity);
	}
	
	function is_path($path){
		if(!file_exists($path)){
			$files = preg_split('/[\/|\\\]/s', $path);
			$_dir = '';
			foreach ($files as $value) {
				$_dir .= $value.DIRECTORY_SEPARATOR;
				if ( !file_exists($_dir) ) {
					if(mkdir($_dir)){
						chmod($_dir,0777);
					}else{
						exit("创建文件夹错误");
					}
				}
			}
		}
	}
	
	function image_text($dst_path,$data){
		$dst = imagecreatefromjpeg($dst_path);
		foreach ($data as $v) {
			$c = explode(',', $v['fontcolor']);
			$w = explode(',',$v['fontplace']);
			$color = imagecolorallocate($dst,$c[0],$c[1],$c[2]);
			imagefttext($dst, $v['fontsize'],0,$w[0],$w[1],$color, FCPATH.$v['font'], $v['fonttext']);
		}
		imagejpeg($dst,$dst_path,95);
		imagedestroy($dst);
	}
	
	protected function isLocalImage($src) {
		//绝对地址
		if ( strpos($src, 'http://') === 0 || strpos($src, 'https://') === 0 ) {
			return false;
		} else {
			return true;
		}
	}
	
	function image_create_from_ext($imgfile){
		$info = getimagesize($imgfile);
		$im = null;
		switch ($info[2]) {
			case 1: $im = imagecreatefromgif($imgfile); break;
			case 2: $im = imagecreatefromjpeg($imgfile); break;
			case 3: $im = imagecreatefrompng($imgfile); break;
		}
		return $im;
	}
	
	function resizejpg($imgsrc,$filename,$imgwidth,$imgheight,$radius = 0)	{
		
		$arr = getimagesize($imgsrc);
		$imgWidth = $imgwidth;
		$imgHeight = $imgheight;
		switch ($arr[2]) {
			case 1: $imgsrc = imagecreatefromgif($imgsrc); break;
			case 2: $imgsrc = imagecreatefromjpeg($imgsrc); break;
			case 3: $imgsrc = imagecreatefrompng($imgsrc); break;
		}
		if($radius){//圆形  圆形一般传入宽度
			$ext     = pathinfo($imgpath);
			$src_img = $imgsrc;			
			$wh = $arr;
			$w  = $imgwidth;
			$h  = $imgheight;
			// $radius = $radius == 0 ? (min($w, $h) / 2) : $radius;
			$img = imagecreatetruecolor($w, $h);
			//这一句一定要有
			imagesavealpha($img, true);
			//拾取一个完全透明的颜色,最后一个参数127为全透明
			$bg = imagecolorallocatealpha($img, 255, 255, 255, 127);
			imagefill($img, 0, 0, $bg);
			$r = $radius/2; //圆 角半径
			for ($x = 0; $x < $w; $x++) {
				for ($y = 0; $y < $h; $y++) {
					$rgbColor = imagecolorat($src_img, $x, $y);
					if (($x >= $radius && $x <= ($w - $radius)) || ($y >= $radius && $y <= ($h - $radius))) {
						//不在四角的范围内,直接画
						imagesetpixel($img, $x, $y, $rgbColor);
					} else {
						//在四角的范围内选择画
						//上左
						$y_x = $r; //圆心X坐标
						$y_y = $r; //圆心Y坐标
						if (((($x - $y_x) * ($x - $y_x) + ($y - $y_y) * ($y - $y_y)) <= ($r * $r))) {
							imagesetpixel($img, $x, $y, $rgbColor);
						}
						//上右
						$y_x = $w - $r; //圆心X坐标
						$y_y = $r; //圆心Y坐标
						if (((($x - $y_x) * ($x - $y_x) + ($y - $y_y) * ($y - $y_y)) <= ($r * $r))) {
							imagesetpixel($img, $x, $y, $rgbColor);
						}
						//下左
						$y_x = $r; //圆心X坐标
						$y_y = $h - $r; //圆心Y坐标
						if (((($x - $y_x) * ($x - $y_x) + ($y - $y_y) * ($y - $y_y)) <= ($r * $r))) {
							imagesetpixel($img, $x, $y, $rgbColor);
						}
						//下右
						$y_x = $w - $r; //圆心X坐标
						$y_y = $h - $r; //圆心Y坐标
						if (((($x - $y_x) * ($x - $y_x) + ($y - $y_y) * ($y - $y_y)) <= ($r * $r))) {
							imagesetpixel($img, $x, $y, $rgbColor);
						}
					}
				}
			}			
			imagepng($imgg,$filename);
			imagedestroy($imgg);
		}else{
			if($arr[2]==3){
				$image = imagecreate($imgWidth, $imgHeight);  //创建一个彩色的底图
			}else{
				$image = imagecreatetruecolor($imgWidth, $imgHeight);
			}
			imagecopyresampled($image, $imgsrc, 0, 0, 0, 0,$imgWidth,$imgHeight,$arr[0], $arr[1]);
			imagepng($image,$filename);
			imagedestroy($image);
		}
		
		
	}
	
	public function downImage($url,$filename){
		$imageinfo = $this->httpImage($url);
		$local_file = fopen($filename, 'w');
		if(false!==$local_file){
			if(false!==fwrite($local_file, $imageinfo['body'])){
				fclose($local_file);
			}
		}
	}
	
	private  function httpImage($url){
		$ch=curl_init($url);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_NOBODY,0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$package=curl_exec($ch);
		$httpinfo=curl_getinfo($ch);
		curl_close($ch);
		return array_merge(array('body'=>$package),array('header'=>$httpinfo));
	}
}