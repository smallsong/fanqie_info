<?php
if (! defined ( 'BASEPATH' ))exit ( 'No direct script access allowed' );
/**
 * 后台公共类
 * @author chaituan@126.com
 */
class AdminCommon extends CI_Controller {
	
	protected $loginUser; // 管理员信息
	public function __construct() {
		parent::__construct ();
		// 处理url的参数问题
		parseURL($this->uri->segment( 4 ));
		// 实例化菜单数据模型
		$this->load->model(array('admin/AdminMenuGroup_model','admin/AdminMenu_model','admin/AdminUser_model' )); // 加载数据模型
		// 判断登录状态
		$this->loginUser = $this->AdminUser_model->get_LoginUser ();
		if (!$this->AdminUser_model->get_LoginUser ()) {
			redirect (site_url('adminct/login/index'));
		} else {
			$this->load->vars('loginUser', $this->loginUser );
		}
		
		// 获取菜单分组缓存
		$groupCache = $this->AdminMenuGroup_model->getGroupCache();
		
		$__menuGroups = changeArrayKey ( $groupCache, 'id' );
		
		// 初始化左侧菜单的选中状态
		$sort_num = array ();
		foreach ( $__menuGroups as $_m ) {
			$sort_num [] = $_m ['sort_num'];
		}
		
		array_multisort ( $sort_num, SORT_ASC, $__menuGroups );
		$systemMenu = $this->AdminMenu_model->getMenuByUser ( $this->loginUser );
		if(!$systemMenu['system']){//如果系统菜单关闭显示 ，则默认展开下一个
			$current = next ( $__menuGroups );
		}else{
			$current = current ( $__menuGroups );
		}
		
		// 获取mid
		$mid = Gets ('m');
		
		if ($mid > 0) {
			$this->session->set_userdata ( 'm', $mid );
			$menu = $this->AdminMenu_model->getItem ( "id=$mid" );
			$mpid = $menu ['pid'];
			$mgroup = $menu ['groupkey'];
			$this->session->set_userdata ( 'mpid', $mpid );
			$this->session->set_userdata ( 'mgroup', $mgroup );
		} else {
			$mpid = $this->session->mpid;
			$mgroup = $this->session->mgroup ? $this->session->mgroup : $current ['tkey'];
			$mid = $this->session->m;
		}
		// 获取菜单数据
		$permissions = $this->AdminUser_model->getPermissions ();
		$this->load->vars ( '__menuGroups', $__menuGroups );
		$this->load->vars ( 'systemMenu', $systemMenu );
		$this->load->vars ( 'mpid', $mpid );
		$this->load->vars ( 'mgroup', $mgroup );
		$this->load->vars ( 'mid', $mid );
		$this->load->vars ( 'add_url', '/' . $this->router->directory . $this->router->class . '/add' );
		$this->load->vars ( 'edit_url', '/' . $this->router->directory . $this->router->class . '/edit' );
		$this->load->vars ( 'index_url', '/' . $this->router->directory . $this->router->class . '/index' );
		$currentOpt = '/' . $this->router->directory . $this->router->class . '/' . $this->router->method;
		$this->load->vars ('currentOpt', $currentOpt);
		$this->load->vars ('dr_url', '/' .$this->router->directory . $this->router->class);//首页常用链接
		
		// 权限认证
		$opt = $this->router->class . '@' . $this->router->method;
		if(!$this->AdminUser_model->isSuperManager($this->loginUser)){
			if (! $this->AdminUser_model->hasPermission ( $opt, $permissions )) {
				showmessage("您没有权限进行该操作！",'error','#');
			}
		}
		$this->load->vars('emptyRecord', 'O(∩_∩)O~ 抱歉，暂无记录！');
	}
}
