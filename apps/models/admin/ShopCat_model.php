<?php
if (! defined ( 'BASEPATH' ))exit ( 'No direct script access allowed' );
/**
 * 分组
 * @author chaituan@126.com
 */
class ShopCat_model extends MY_Model {
	function __construct() {
		parent::__construct ();
		$this->table_name = 'shop_cat';
	}
	
	function cache() {
		$items = $this->getItems();
		set_Cache('shop_cat',$items);
	}
}