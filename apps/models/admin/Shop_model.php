<?php
if (! defined ( 'BASEPATH' ))exit ( 'No direct script access allowed' );
/**
 * 商家管理
 * @author chaituan@126.com
 */
class Shop_model extends MY_Model {
	function __construct() {
		parent::__construct ();
		$this->table_name = 'shop';
	}
}