<?php echo template('admin/header');echo template('admin/sider');?>
<div class="layui-body">
	<div class="childrenBody childrenBody_show">
		<blockquote class="layui-elem-quote a-e-quote">
				<div class="layui-inline">添加</div>
				<div class="layui-inline f-right"><?php echo admin_btn($index_url, '', 'layui-btn-xs','','返回')?></div>
		</blockquote>
		<form class="layui-form  a-e-form" method="post">
			<div class="layui-form-item">
				<label class="layui-form-label">商家名字</label>
				<div class="layui-input-block">
					<input type="text" name="data[sname]" class="layui-input" lay-verify="required">
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">头像</label>
				<div class="layui-input-block">
					<?php echo admin_btn(site_url('/images/upload'), 'file','layui-btn-primary f_file');?><span class="thumb-say">80px * 80px</span>
					<div class="layui-upload layui-hide">
					  	<div class="layui-upload-list"><table class="layui-table"><tbody class="file_list"></tbody></table></div>
					  	<?php echo admin_btn('', 'file_sub','layui-btn-normal','');?>
				  	</div>
				  	<input name="data[thumb]" class="thumb" type="hidden" lay-verify="thumb"/>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">帐号</label>
				<div class="layui-input-block">
					<input type="text" name="data[username]" class="layui-input" lay-verify="required">
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">密码</label>
				<div class="layui-input-block">
					<input type="text" name="data[pwd]" class="layui-input" lay-verify="required">
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">商家坐标</label>
				<div class="layui-input-inline">
					<input type="text" name="data[zb]" class="layui-input" lay-verify="required">
				</div>
				<div class="layui-form-mid layui-word-aux"><a href="http://lbs.qq.com/tool/getpoint/" target="_blank">获取坐标</a>（复制当前坐标到此处即可）</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">详细地址</label>
				<div class="layui-input-block">
					<input type="text" name="data[address]" class="layui-input" lay-verify="required">
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">联系电话</label>
				<div class="layui-input-block">
					<input type="text" name="data[mobile]" class="layui-input" lay-verify="required">
				</div>
			</div>
			<div class="layui-form-item">
				<div class="layui-input-block">
					<?php echo admin_btn($add_url,'save','layui-btn-lg',"lay-filter='sub' location='$index_url'")?>
				</div>
			</div>
	</form>
	</div>
</div>
<?php echo template('admin/script');?>
<script type="text/javascript" src="<?php echo JS_PATH.'f_upload.js'?>" ></script>
<script type="text/javascript">
$(function(){
	$('.f_file').f_upload();
});
</script>
<?php echo template('admin/footer');?>