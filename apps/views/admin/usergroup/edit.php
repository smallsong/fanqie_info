<?php echo template('admin/header');echo template('admin/sider');?>
<div class="layui-body">
	<div class="childrenBody childrenBody_show">
		<blockquote class="layui-elem-quote a-e-quote">
				<div class="layui-inline">编辑</div>
				<div class="layui-inline f-right"><?php echo admin_btn($index_url, '', 'layui-btn-xs','','返回')?></div>
		</blockquote>
		<form class="layui-form  a-e-form" method="post">
			<div class="layui-form-item">
				<label class="layui-form-label">名称</label>
				<div class="layui-input-block">
					<input type="text" name="data[gname]" class="layui-input" value="<?php echo $item['gname']?>" lay-verify="required">
				</div>
			</div>
			
			<div class="layui-form-item">
				<label class="layui-form-label">粉丝数量</label>
				<div class="layui-input-block">
					<input type="text" name="data[fans_num]" class="layui-input"  value="<?php echo $item['fans_num']?>" lay-verify="required">
				</div>
			</div>
			
			<div class="layui-form-item">
				<label class="layui-form-label">一级</label>
				<div class="layui-input-block">
					<div class="input-group">
						<input type="text" name="data[p_1]" class="layui-input" value="<?php echo $item['p_1']?>" lay-verify="required">
					</div>
				</div>
			</div>
			
			<div class="layui-form-item">
				<label class="layui-form-label">二级</label>
				<div class="layui-input-block">
					<div class="input-group">
						<input type="text" name="data[p_2]" class="layui-input"  value="<?php echo $item['p_2']?>" lay-verify="required">
					</div>
				</div>
			</div>
			
			<div class="layui-form-item">
				<label class="layui-form-label">说明</label>
				<div class="layui-input-block">
					<input type="text" name="data[summary]" class="layui-input"  value="<?php echo $item['summary']?>" lay-verify="required">
				</div>
			</div>
			
			<div class="layui-form-item">
				
				<div class="layui-input-block">
				<input type="hidden" name="id" value="<?php echo $item['id']?>">
				<?php echo admin_btn($edit_url,'save','layui-btn-lg',"lay-filter='sub' location='$index_url'")?>
				</div>
			</div>
		</form>
	</div>
</div>
<?php echo template('admin/script');?>
<?php echo template('admin/footer');?>