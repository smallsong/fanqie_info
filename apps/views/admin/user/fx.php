<?php echo template('admin/header');echo template('admin/sider');?>
<div class="layui-body">
	<div class="childrenBody childrenBody_show">
		<blockquote class="layui-elem-quote news_search">
				<div class="layui-inline">
					<form class="layui-form">
					<div class="layui-input-inline">
						<input type="text"  id="table-find-val" placeholder="请输入昵称" class="layui-input" lay-verify='required'>
					</div>
				    <?php echo admin_btn('', 'find',"",'lay-filter="table-find"')?>
					</form>
				</div>
		</blockquote>
		<table  id="user" lay-filter="common" ></table>
	</div>
</div>


<?php echo template('admin/script');?>
<script type="text/html" id="operation">
<?php echo admin_btn(site_url($dr_url.'/sh_ok/id-{{d.id}}'),'del','layui-btn-xs layui-bg-green f_del_d','lay-event="del"','通过');?>
<?php echo admin_btn(site_url($dr_url.'/sh_no/id-{{d.id}}'),'del','layui-btn-xs layui-bg-orange f_del_d','lay-event="del"','拒绝');?>
<?php echo admin_btn(site_url($dr_url.'/fx_del/id-{{d.id}}'),'del','layui-btn-xs f_del_d','lay-event="del"');?>
</script>
<script>
//执行渲染
var tab = layui.table.render({
	elem: '#user', //指定原始表格元素选择器（推荐id选择器）
	id:'common',//给事件用的
	height: 'full-250', //容器高度
	url:'<?php echo site_url("$dr_url/fx_lists")?>',
	cols: [[
	       {checkbox: true},
	       {type:'numbers',title: 'No.'},
	       {field: 'id', title: 'ID', width: 80,sort:true},
	       {field: 'nicknames', title: '昵称'},
	       {field:'thumb',title:'头像',toolbar:'<div><div class="img_view"><img src="{{d.thumb}}"></div></div>'},
	       {field: 'addtime', title: '注册时间',toolbar:'<div>{{Time(d.addtime, "%y-%M-%d %h:%m:%s")}}</div>'},
	       {field: 'gid', title: '分销审核',width: 90,toolbar:'<div>{{# if(d.gid==3){ }} <span style="color:#01AAED">等待审核</span> {{# }else if(d.gid==2){ }} <span style="color:#FF5722">正常</span>  {{# }else{ }}  {{# } }}</div>'},
	       {field: 'right', title: '操作',toolbar: '#operation',width: 150}
	       ]],
	limit: 15,
	page:true,
	response:{msgName:'message'},
	done:function(res, curr, count){
		this.where.total = count;
		layer.photos({photos:'.img_view'});//添加预览
	}
});
</script>

<?php echo template('admin/footer');?>