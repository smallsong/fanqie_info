/*
Navicat MySQL Data Transfer

Source Server         : mysql
Source Server Version : 50635
Source Host           : localhost:3306
Source Database       : hbsq

Target Server Type    : MYSQL
Target Server Version : 50635
File Encoding         : 65001

Date: 2019-08-23 10:31:51
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `ct_ad`
-- ----------------------------
DROP TABLE IF EXISTS `ct_ad`;
CREATE TABLE `ct_ad` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `thumb` varchar(255) DEFAULT NULL COMMENT '图片',
  `url` varchar(255) DEFAULT NULL,
  `sort` int(11) DEFAULT '0',
  `cid` int(11) DEFAULT '0',
  `cidval` int(11) DEFAULT NULL,
  `lid` varchar(255) DEFAULT NULL,
  `gid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='文章';

-- ----------------------------
-- Records of ct_ad
-- ----------------------------
INSERT INTO `ct_ad` VALUES ('1', '首页广告图一', '/res/upload/images/20171207/1512631765814806.jpg', '#', '0', '0', null, null, null);
INSERT INTO `ct_ad` VALUES ('2', '拼团广告一', '/res/upload/images/20171128/1511861677704215.png', '#', '0', '0', null, null, null);
INSERT INTO `ct_ad` VALUES ('3', '首页广告二图1', '/res/upload/images/20171207/1512631830302083.jpg', '/pages/goods/detail?id=2', '1', '2', null, null, null);
INSERT INTO `ct_ad` VALUES ('7', '少时诵诗书', '/res/upload/images/20180427/1524843386568966.jpg', '/pages/lists/index?id=2', null, '1', null, null, null);
INSERT INTO `ct_ad` VALUES ('8', '测试', '/res/upload/images/20180805/1533462394512336.jpg', '/pages/lists/index?id=#&tj=0', '0', '1', null, '1,2', '1');

-- ----------------------------
-- Table structure for `ct_admin_menu`
-- ----------------------------
DROP TABLE IF EXISTS `ct_admin_menu`;
CREATE TABLE `ct_admin_menu` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `groupkey` varchar(20) DEFAULT '0' COMMENT '菜单分组key',
  `name` varchar(10) NOT NULL DEFAULT '' COMMENT '名称',
  `url` varchar(100) NOT NULL DEFAULT '' COMMENT '链接地址',
  `pid` smallint(4) DEFAULT '0' COMMENT '父菜单',
  `ishow` tinyint(1) DEFAULT '1' COMMENT '0：不显示，1：显示，默认为1',
  `sort_num` smallint(3) DEFAULT '0' COMMENT '排序数字',
  `add_time` int(11) DEFAULT '0' COMMENT '添加时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COMMENT='系统后台菜单表';

-- ----------------------------
-- Records of ct_admin_menu
-- ----------------------------
INSERT INTO `ct_admin_menu` VALUES ('1', 'system', '系统管理', '#', '0', '0', '1', '1431589378');
INSERT INTO `ct_admin_menu` VALUES ('2', 'system', '系统配置', '/adminct/config/index', '1', '1', '2', '1431593502');
INSERT INTO `ct_admin_menu` VALUES ('3', 'system', '菜单管理', '/adminct/menu/index', '1', '1', '3', '1431595612');
INSERT INTO `ct_admin_menu` VALUES ('4', 'system', '菜单分组', '/adminct/menugroup/index', '1', '1', '4', '1431597951');
INSERT INTO `ct_admin_menu` VALUES ('8', 'user', '会员管理', 'adminct/user/index', '7', '1', '2', '1523931742');
INSERT INTO `ct_admin_menu` VALUES ('7', 'user', '会员管理', '#', '0', '1', '1', '1523931713');
INSERT INTO `ct_admin_menu` VALUES ('9', 'web', '站点管理', '#', '0', '1', '1', '1524053775');
INSERT INTO `ct_admin_menu` VALUES ('10', 'web', '区域管理', 'adminct/location/index', '9', '1', '2', '1524053875');
INSERT INTO `ct_admin_menu` VALUES ('11', 'web', '商家管理', 'adminct/shop/index', '9', '1', '3', '1524053899');
INSERT INTO `ct_admin_menu` VALUES ('12', 'web', '首页导航', 'adminct/nav/index', '9', '1', '4', '1524053978');
INSERT INTO `ct_admin_menu` VALUES ('13', 'goods', '商品管理', '#', '0', '1', '1', '1524054335');
INSERT INTO `ct_admin_menu` VALUES ('14', 'goods', '商品管理', 'adminct/goods/index', '13', '1', '2', '1524054433');
INSERT INTO `ct_admin_menu` VALUES ('15', 'goods', '订单管理', 'adminct/order/index', '13', '1', '3', '1524054457');
INSERT INTO `ct_admin_menu` VALUES ('16', 'user', '积分管理', 'adminct/integral/index', '7', '1', '3', '1524054522');
INSERT INTO `ct_admin_menu` VALUES ('17', 'web', '广告管理', 'adminct/ad/index', '9', '1', '5', '1524054557');
INSERT INTO `ct_admin_menu` VALUES ('18', 'goods', '商品分类', 'adminct/goods_group/index', '13', '1', '4', '1524064000');
INSERT INTO `ct_admin_menu` VALUES ('19', 'user', '分销管理', 'adminct/user/fx', '7', '1', '4', '1527166046');

-- ----------------------------
-- Table structure for `ct_admin_menu_group`
-- ----------------------------
DROP TABLE IF EXISTS `ct_admin_menu_group`;
CREATE TABLE `ct_admin_menu_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tkey` varchar(20) DEFAULT '' COMMENT '分组key,需要保持唯一',
  `name` varchar(20) DEFAULT '' COMMENT '分组名称',
  `icon` varchar(20) DEFAULT '' COMMENT '分组图标',
  `sort_num` smallint(3) DEFAULT '0' COMMENT '排序数字',
  `add_time` int(11) DEFAULT '0' COMMENT '添加时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='后台菜单分组';

-- ----------------------------
-- Records of ct_admin_menu_group
-- ----------------------------
INSERT INTO `ct_admin_menu_group` VALUES ('1', 'system', '系统', 'cog', '1', '1431586979');
INSERT INTO `ct_admin_menu_group` VALUES ('2', 'user', '会员', 'user', '2', '1523931689');
INSERT INTO `ct_admin_menu_group` VALUES ('3', 'web', '站点', 'internet-explorer', '3', '1524053743');
INSERT INTO `ct_admin_menu_group` VALUES ('4', 'goods', '商品', 'shopping-bag', '4', '1524054311');

-- ----------------------------
-- Table structure for `ct_admin_role`
-- ----------------------------
DROP TABLE IF EXISTS `ct_admin_role`;
CREATE TABLE `ct_admin_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(20) DEFAULT '' COMMENT '角色名称',
  `summary` varchar(100) DEFAULT '' COMMENT '角色简介',
  `permissions` text COMMENT '角色权限的序列表数组',
  `add_time` int(11) DEFAULT '0' COMMENT '添加时间',
  `sort_num` smallint(3) DEFAULT '0' COMMENT '排序数字',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='管理员角色';

-- ----------------------------
-- Records of ct_admin_role
-- ----------------------------
INSERT INTO `ct_admin_role` VALUES ('1', '超级管理员', '超级管理员', '%7B%22adminuser%40index%22%3A%221%22%2C%22adminuser%40add%22%3A%221%22%2C%22adminuser%40edit%22%3A%221%22%2C%22adminuser%40del%22%3A%221%22%2C%22adminuser%40dels%22%3A%221%22%2C%22adminuser%40lock%22%3A%221%22%2C%22adminuser%40edit_pwd%22%3A%221%22%2C%22adminuser%40export%22%3A%221%22%2C%22adminrole%40index%22%3A%221%22%2C%22adminrole%40add%22%3A%221%22%2C%22adminrole%40edit%22%3A%221%22%2C%22adminrole%40del%22%3A%221%22%2C%22adminrole%40permission%22%3A%221%22%2C%22news%40index%22%3A%221%22%2C%22news%40add%22%3A%221%22%2C%22news%40edit%22%3A%221%22%2C%22news%40del%22%3A%221%22%2C%22news%40dels%22%3A%221%22%2C%22news%40lock%22%3A%221%22%2C%22newsgroup%40index%22%3A%221%22%2C%22newsgroup%40add%22%3A%221%22%2C%22newsgroup%40edit%22%3A%221%22%2C%22newsgroup%40del%22%3A%221%22%2C%22user%40index%22%3A%221%22%2C%22user%40del%22%3A%221%22%2C%22user%40dels%22%3A%221%22%2C%22user%40lock%22%3A%221%22%2C%22user%40config%22%3A%221%22%2C%22user%40export%22%3A%221%22%2C%22usergroup%40index%22%3A%221%22%2C%22usergroup%40add%22%3A%221%22%2C%22usergroup%40del%22%3A%221%22%7D', '1431505208', '1');
INSERT INTO `ct_admin_role` VALUES ('2', '后台管理员', '普通管理员', '%7B%22adminuser%40index%22%3A%221%22%2C%22adminuser%40add%22%3A%221%22%2C%22adminuser%40edit%22%3A%221%22%2C%22adminuser%40del%22%3A%221%22%2C%22adminuser%40lock%22%3A%221%22%2C%22adminuser%40edit_pwd%22%3A%221%22%2C%22adminuser%40export%22%3A%221%22%2C%22user%40index%22%3A%221%22%2C%22user%40del%22%3A%221%22%2C%22user%40dels%22%3A%221%22%2C%22user%40lock%22%3A%221%22%2C%22user%40export%22%3A%221%22%7D', '0', '0');

-- ----------------------------
-- Table structure for `ct_admin_user`
-- ----------------------------
DROP TABLE IF EXISTS `ct_admin_user`;
CREATE TABLE `ct_admin_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `role_id` int(11) unsigned DEFAULT '0' COMMENT '角色ID',
  `username` varchar(30) DEFAULT '' COMMENT '用户名',
  `name` varchar(20) DEFAULT '' COMMENT '姓名',
  `password` varchar(32) DEFAULT '' COMMENT '密码',
  `last_login_time` int(11) DEFAULT '0' COMMENT '最后一次登录时间',
  `last_login_ip` varchar(15) DEFAULT '' COMMENT '最后一次登录ip',
  `add_time` int(11) DEFAULT '0' COMMENT '注册时间',
  `state` tinyint(1) unsigned DEFAULT '1' COMMENT '当前状态(0=>关闭，1=>开启) 默认1开启',
  `isystem` tinyint(1) DEFAULT '0' COMMENT '是否系统管理员(系统管理员不准许删除)',
  `summary` varchar(100) DEFAULT '' COMMENT '管理员简介',
  `encrypt` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='管理员表';

-- ----------------------------
-- Records of ct_admin_user
-- ----------------------------
INSERT INTO `ct_admin_user` VALUES ('1', '0', 'admin', '管理员', 'ad1aed4518b3883cf64cd4417daf7da0', '1532519102', '127.0.0.1', '1431566261', '1', '1', '超级管理员', 'yvq2KT3');
INSERT INTO `ct_admin_user` VALUES ('2', '1', 'admins', '管理员', '6b8f542dfc3ed6d8990025de8e2fc199', '1511349895', '127.0.0.1', '1510281093', '1', '0', '后台管理员', 'gBcm7u1');
INSERT INTO `ct_admin_user` VALUES ('3', '2', 'test', 'test', '281b5f0f8f0832dcfae6258197ff3001', '1511329689', '127.0.0.1', '1510292725', '1', '0', '测试帐号', 'BZEmbiS');

-- ----------------------------
-- Table structure for `ct_ad_group`
-- ----------------------------
DROP TABLE IF EXISTS `ct_ad_group`;
CREATE TABLE `ct_ad_group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `aname` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of ct_ad_group
-- ----------------------------
INSERT INTO `ct_ad_group` VALUES ('1', '抢购首页');

-- ----------------------------
-- Table structure for `ct_card`
-- ----------------------------
DROP TABLE IF EXISTS `ct_card`;
CREATE TABLE `ct_card` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `bm` varchar(255) DEFAULT NULL,
  `position` varchar(255) DEFAULT NULL,
  `zz` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `la` varchar(255) DEFAULT NULL,
  `lg` varchar(255) DEFAULT NULL,
  `iid` int(11) DEFAULT NULL,
  `iname` varchar(255) DEFAULT NULL,
  `cid` int(11) DEFAULT NULL,
  `cname` varchar(255) DEFAULT NULL,
  `thumb` varchar(255) DEFAULT NULL,
  `zan` int(11) DEFAULT '0',
  `follow` int(11) DEFAULT '0',
  `rq` int(11) DEFAULT '0',
  `addtime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of ct_card
-- ----------------------------

-- ----------------------------
-- Table structure for `ct_card_follow`
-- ----------------------------
DROP TABLE IF EXISTS `ct_card_follow`;
CREATE TABLE `ct_card_follow` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `nickname` varchar(255) DEFAULT NULL,
  `header` varchar(255) DEFAULT NULL,
  `position` varchar(255) DEFAULT NULL,
  `o_uid` int(11) DEFAULT NULL,
  `o_nickname` varchar(255) DEFAULT NULL,
  `o_header` varchar(255) DEFAULT NULL,
  `o_position` varchar(255) DEFAULT NULL,
  `addtime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of ct_card_follow
-- ----------------------------

-- ----------------------------
-- Table structure for `ct_card_rq`
-- ----------------------------
DROP TABLE IF EXISTS `ct_card_rq`;
CREATE TABLE `ct_card_rq` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `nickname` varchar(255) DEFAULT NULL,
  `header` varchar(255) DEFAULT NULL,
  `position` varchar(255) DEFAULT NULL,
  `o_uid` int(11) DEFAULT NULL,
  `o_nickname` varchar(255) DEFAULT NULL,
  `o_header` varchar(255) DEFAULT NULL,
  `o_position` varchar(255) DEFAULT NULL,
  `num` int(11) DEFAULT '0',
  `addtime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of ct_card_rq
-- ----------------------------

-- ----------------------------
-- Table structure for `ct_card_zan`
-- ----------------------------
DROP TABLE IF EXISTS `ct_card_zan`;
CREATE TABLE `ct_card_zan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `nickname` varchar(255) DEFAULT NULL,
  `header` varchar(255) DEFAULT NULL,
  `position` varchar(255) DEFAULT NULL,
  `o_uid` int(11) DEFAULT NULL,
  `o_nickname` varchar(255) DEFAULT NULL,
  `o_header` varchar(255) DEFAULT NULL,
  `o_position` varchar(255) DEFAULT NULL,
  `addtime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of ct_card_zan
-- ----------------------------

-- ----------------------------
-- Table structure for `ct_city`
-- ----------------------------
DROP TABLE IF EXISTS `ct_city`;
CREATE TABLE `ct_city` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `cname` varchar(20) NOT NULL COMMENT '地区名称',
  `parent_id` int(11) NOT NULL DEFAULT '0' COMMENT '父级id',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1823 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of ct_city
-- ----------------------------
INSERT INTO `ct_city` VALUES ('18', '北京市', '0');
INSERT INTO `ct_city` VALUES ('2', '天津市', '0');
INSERT INTO `ct_city` VALUES ('3', '上海市', '0');
INSERT INTO `ct_city` VALUES ('4', '重庆市', '0');
INSERT INTO `ct_city` VALUES ('5', '河北省', '0');
INSERT INTO `ct_city` VALUES ('6', '山西省', '0');
INSERT INTO `ct_city` VALUES ('7', '台湾省', '0');
INSERT INTO `ct_city` VALUES ('8', '辽宁省', '0');
INSERT INTO `ct_city` VALUES ('9', '吉林省', '0');
INSERT INTO `ct_city` VALUES ('10', '黑龙江省', '0');
INSERT INTO `ct_city` VALUES ('11', '江苏省', '0');
INSERT INTO `ct_city` VALUES ('12', '浙江省', '0');
INSERT INTO `ct_city` VALUES ('13', '安徽省', '0');
INSERT INTO `ct_city` VALUES ('14', '福建省', '0');
INSERT INTO `ct_city` VALUES ('15', '江西省', '0');
INSERT INTO `ct_city` VALUES ('16', '山东省', '0');
INSERT INTO `ct_city` VALUES ('17', '河南省', '0');
INSERT INTO `ct_city` VALUES ('1', '湖北省', '0');
INSERT INTO `ct_city` VALUES ('19', '湖南省', '0');
INSERT INTO `ct_city` VALUES ('20', '广东省', '0');
INSERT INTO `ct_city` VALUES ('21', '甘肃省', '0');
INSERT INTO `ct_city` VALUES ('22', '四川省', '0');
INSERT INTO `ct_city` VALUES ('23', '贵州省', '0');
INSERT INTO `ct_city` VALUES ('24', '海南省', '0');
INSERT INTO `ct_city` VALUES ('25', '云南省', '0');
INSERT INTO `ct_city` VALUES ('26', '青海省', '0');
INSERT INTO `ct_city` VALUES ('27', '陕西省', '0');
INSERT INTO `ct_city` VALUES ('28', '广西壮族自治区', '0');
INSERT INTO `ct_city` VALUES ('29', '西藏自治区', '0');
INSERT INTO `ct_city` VALUES ('30', '宁夏回族自治区', '0');
INSERT INTO `ct_city` VALUES ('31', '新疆维吾尔自治区', '0');
INSERT INTO `ct_city` VALUES ('32', '内蒙古自治区', '0');
INSERT INTO `ct_city` VALUES ('33', '澳门特别行政区', '0');
INSERT INTO `ct_city` VALUES ('34', '香港特别行政区', '0');
INSERT INTO `ct_city` VALUES ('35', '北京市', '18');
INSERT INTO `ct_city` VALUES ('36', '天津市', '2');
INSERT INTO `ct_city` VALUES ('37', '上海市', '3');
INSERT INTO `ct_city` VALUES ('38', '重庆市', '4');
INSERT INTO `ct_city` VALUES ('39', '石家庄市', '5');
INSERT INTO `ct_city` VALUES ('40', '唐山市', '5');
INSERT INTO `ct_city` VALUES ('41', '秦皇岛市', '5');
INSERT INTO `ct_city` VALUES ('42', '邯郸市', '5');
INSERT INTO `ct_city` VALUES ('43', '邢台市', '5');
INSERT INTO `ct_city` VALUES ('44', '保定市', '5');
INSERT INTO `ct_city` VALUES ('45', '张家口市', '5');
INSERT INTO `ct_city` VALUES ('46', '承德市', '5');
INSERT INTO `ct_city` VALUES ('47', '沧州市', '5');
INSERT INTO `ct_city` VALUES ('48', '廊坊市', '5');
INSERT INTO `ct_city` VALUES ('49', '衡水市', '5');
INSERT INTO `ct_city` VALUES ('50', '太原市', '6');
INSERT INTO `ct_city` VALUES ('51', '大同市', '6');
INSERT INTO `ct_city` VALUES ('52', '阳泉市', '6');
INSERT INTO `ct_city` VALUES ('53', '长治市', '6');
INSERT INTO `ct_city` VALUES ('54', '晋城市', '6');
INSERT INTO `ct_city` VALUES ('55', '朔州市', '6');
INSERT INTO `ct_city` VALUES ('56', '晋中市', '6');
INSERT INTO `ct_city` VALUES ('57', '运城市', '6');
INSERT INTO `ct_city` VALUES ('58', '忻州市', '6');
INSERT INTO `ct_city` VALUES ('59', '临汾市', '6');
INSERT INTO `ct_city` VALUES ('60', '吕梁市', '6');
INSERT INTO `ct_city` VALUES ('61', '台北市', '7');
INSERT INTO `ct_city` VALUES ('62', '高雄市', '7');
INSERT INTO `ct_city` VALUES ('63', '基隆市', '7');
INSERT INTO `ct_city` VALUES ('64', '台中市', '7');
INSERT INTO `ct_city` VALUES ('65', '台南市', '7');
INSERT INTO `ct_city` VALUES ('66', '新竹市', '7');
INSERT INTO `ct_city` VALUES ('67', '嘉义市', '7');
INSERT INTO `ct_city` VALUES ('68', '台北县', '7');
INSERT INTO `ct_city` VALUES ('69', '宜兰县', '7');
INSERT INTO `ct_city` VALUES ('70', '桃园县', '7');
INSERT INTO `ct_city` VALUES ('71', '新竹县', '7');
INSERT INTO `ct_city` VALUES ('72', '苗栗县', '7');
INSERT INTO `ct_city` VALUES ('73', '台中县', '7');
INSERT INTO `ct_city` VALUES ('74', '彰化县', '7');
INSERT INTO `ct_city` VALUES ('75', '南投县', '7');
INSERT INTO `ct_city` VALUES ('76', '云林县', '7');
INSERT INTO `ct_city` VALUES ('77', '嘉义县', '7');
INSERT INTO `ct_city` VALUES ('78', '台南县', '7');
INSERT INTO `ct_city` VALUES ('79', '高雄县', '7');
INSERT INTO `ct_city` VALUES ('80', '屏东县', '7');
INSERT INTO `ct_city` VALUES ('81', '澎湖县', '7');
INSERT INTO `ct_city` VALUES ('82', '台东县', '7');
INSERT INTO `ct_city` VALUES ('83', '花莲县', '7');
INSERT INTO `ct_city` VALUES ('84', '沈阳市', '8');
INSERT INTO `ct_city` VALUES ('85', '大连市', '8');
INSERT INTO `ct_city` VALUES ('86', '鞍山市', '8');
INSERT INTO `ct_city` VALUES ('87', '抚顺市', '8');
INSERT INTO `ct_city` VALUES ('88', '本溪市', '8');
INSERT INTO `ct_city` VALUES ('89', '丹东市', '8');
INSERT INTO `ct_city` VALUES ('90', '锦州市', '8');
INSERT INTO `ct_city` VALUES ('91', '营口市', '8');
INSERT INTO `ct_city` VALUES ('92', '阜新市', '8');
INSERT INTO `ct_city` VALUES ('93', '辽阳市', '8');
INSERT INTO `ct_city` VALUES ('94', '盘锦市', '8');
INSERT INTO `ct_city` VALUES ('95', '铁岭市', '8');
INSERT INTO `ct_city` VALUES ('96', '朝阳市', '8');
INSERT INTO `ct_city` VALUES ('97', '葫芦岛市', '8');
INSERT INTO `ct_city` VALUES ('98', '长春市', '9');
INSERT INTO `ct_city` VALUES ('99', '吉林市', '9');
INSERT INTO `ct_city` VALUES ('100', '四平市', '9');
INSERT INTO `ct_city` VALUES ('101', '辽源市', '9');
INSERT INTO `ct_city` VALUES ('102', '通化市', '9');
INSERT INTO `ct_city` VALUES ('103', '白山市', '9');
INSERT INTO `ct_city` VALUES ('104', '松原市', '9');
INSERT INTO `ct_city` VALUES ('105', '白城市', '9');
INSERT INTO `ct_city` VALUES ('106', '延边朝鲜族自治州', '9');
INSERT INTO `ct_city` VALUES ('107', '哈尔滨市', '10');
INSERT INTO `ct_city` VALUES ('108', '齐齐哈尔市', '10');
INSERT INTO `ct_city` VALUES ('109', '鹤岗市', '10');
INSERT INTO `ct_city` VALUES ('110', '双鸭山市', '10');
INSERT INTO `ct_city` VALUES ('111', '鸡西市', '10');
INSERT INTO `ct_city` VALUES ('112', '大庆市', '10');
INSERT INTO `ct_city` VALUES ('113', '伊春市', '10');
INSERT INTO `ct_city` VALUES ('114', '牡丹江市', '10');
INSERT INTO `ct_city` VALUES ('115', '佳木斯市', '10');
INSERT INTO `ct_city` VALUES ('116', '七台河市', '10');
INSERT INTO `ct_city` VALUES ('117', '黑河市', '10');
INSERT INTO `ct_city` VALUES ('118', '绥化市', '10');
INSERT INTO `ct_city` VALUES ('119', '大兴安岭地区', '10');
INSERT INTO `ct_city` VALUES ('120', '苏州市', '11');
INSERT INTO `ct_city` VALUES ('121', '南通市', '11');
INSERT INTO `ct_city` VALUES ('122', '连云港市', '11');
INSERT INTO `ct_city` VALUES ('123', '淮安市', '11');
INSERT INTO `ct_city` VALUES ('124', '盐城市', '11');
INSERT INTO `ct_city` VALUES ('125', '扬州市', '11');
INSERT INTO `ct_city` VALUES ('126', '镇江市', '11');
INSERT INTO `ct_city` VALUES ('127', '泰州市', '11');
INSERT INTO `ct_city` VALUES ('128', '宿迁市', '11');
INSERT INTO `ct_city` VALUES ('129', '南京市', '11');
INSERT INTO `ct_city` VALUES ('130', '无锡市', '11');
INSERT INTO `ct_city` VALUES ('131', '徐州市', '11');
INSERT INTO `ct_city` VALUES ('132', '常州市', '11');
INSERT INTO `ct_city` VALUES ('133', '杭州市', '12');
INSERT INTO `ct_city` VALUES ('134', '宁波市', '12');
INSERT INTO `ct_city` VALUES ('135', '温州市', '12');
INSERT INTO `ct_city` VALUES ('136', '嘉兴市', '12');
INSERT INTO `ct_city` VALUES ('137', '湖州市', '12');
INSERT INTO `ct_city` VALUES ('138', '绍兴市', '12');
INSERT INTO `ct_city` VALUES ('139', '金华市', '12');
INSERT INTO `ct_city` VALUES ('140', '衢州市', '12');
INSERT INTO `ct_city` VALUES ('141', '舟山市', '12');
INSERT INTO `ct_city` VALUES ('142', '台州市', '12');
INSERT INTO `ct_city` VALUES ('143', '丽水市', '12');
INSERT INTO `ct_city` VALUES ('144', '合肥市', '13');
INSERT INTO `ct_city` VALUES ('145', '芜湖市', '13');
INSERT INTO `ct_city` VALUES ('146', '蚌埠市', '13');
INSERT INTO `ct_city` VALUES ('147', '淮南市', '13');
INSERT INTO `ct_city` VALUES ('148', '马鞍山市', '13');
INSERT INTO `ct_city` VALUES ('149', '淮北市', '13');
INSERT INTO `ct_city` VALUES ('150', '铜陵市', '13');
INSERT INTO `ct_city` VALUES ('151', '安庆市', '13');
INSERT INTO `ct_city` VALUES ('152', '黄山市', '13');
INSERT INTO `ct_city` VALUES ('153', '滁州市', '13');
INSERT INTO `ct_city` VALUES ('154', '阜阳市', '13');
INSERT INTO `ct_city` VALUES ('155', '宿州市', '13');
INSERT INTO `ct_city` VALUES ('156', '巢湖市', '13');
INSERT INTO `ct_city` VALUES ('157', '六安市', '13');
INSERT INTO `ct_city` VALUES ('158', '亳州市', '13');
INSERT INTO `ct_city` VALUES ('159', '池州市', '13');
INSERT INTO `ct_city` VALUES ('160', '宣城市', '13');
INSERT INTO `ct_city` VALUES ('161', '福州市', '14');
INSERT INTO `ct_city` VALUES ('162', '厦门市', '14');
INSERT INTO `ct_city` VALUES ('163', '莆田市', '14');
INSERT INTO `ct_city` VALUES ('164', '三明市', '14');
INSERT INTO `ct_city` VALUES ('165', '泉州市', '14');
INSERT INTO `ct_city` VALUES ('166', '漳州市', '14');
INSERT INTO `ct_city` VALUES ('167', '南平市', '14');
INSERT INTO `ct_city` VALUES ('168', '龙岩市', '14');
INSERT INTO `ct_city` VALUES ('169', '宁德市', '14');
INSERT INTO `ct_city` VALUES ('170', '南昌市', '15');
INSERT INTO `ct_city` VALUES ('171', '景德镇市', '15');
INSERT INTO `ct_city` VALUES ('172', '萍乡市', '15');
INSERT INTO `ct_city` VALUES ('173', '九江市', '15');
INSERT INTO `ct_city` VALUES ('174', '新余市', '15');
INSERT INTO `ct_city` VALUES ('175', '鹰潭市', '15');
INSERT INTO `ct_city` VALUES ('176', '赣州市', '15');
INSERT INTO `ct_city` VALUES ('177', '吉安市', '15');
INSERT INTO `ct_city` VALUES ('178', '宜春市', '15');
INSERT INTO `ct_city` VALUES ('179', '抚州市', '15');
INSERT INTO `ct_city` VALUES ('180', '上饶市', '15');
INSERT INTO `ct_city` VALUES ('181', '济南市', '16');
INSERT INTO `ct_city` VALUES ('182', '青岛市', '16');
INSERT INTO `ct_city` VALUES ('183', '淄博市', '16');
INSERT INTO `ct_city` VALUES ('184', '枣庄市', '16');
INSERT INTO `ct_city` VALUES ('185', '东营市', '16');
INSERT INTO `ct_city` VALUES ('186', '烟台市', '16');
INSERT INTO `ct_city` VALUES ('187', '潍坊市', '16');
INSERT INTO `ct_city` VALUES ('188', '济宁市', '16');
INSERT INTO `ct_city` VALUES ('189', '泰安市', '16');
INSERT INTO `ct_city` VALUES ('190', '威海市', '16');
INSERT INTO `ct_city` VALUES ('191', '日照市', '16');
INSERT INTO `ct_city` VALUES ('192', '莱芜市', '16');
INSERT INTO `ct_city` VALUES ('193', '临沂市', '16');
INSERT INTO `ct_city` VALUES ('194', '德州市', '16');
INSERT INTO `ct_city` VALUES ('195', '聊城市', '16');
INSERT INTO `ct_city` VALUES ('196', '滨州市', '16');
INSERT INTO `ct_city` VALUES ('197', '菏泽市', '16');
INSERT INTO `ct_city` VALUES ('198', '郑州市', '17');
INSERT INTO `ct_city` VALUES ('199', '开封市', '17');
INSERT INTO `ct_city` VALUES ('200', '洛阳市', '17');
INSERT INTO `ct_city` VALUES ('201', '平顶山市', '17');
INSERT INTO `ct_city` VALUES ('202', '安阳市', '17');
INSERT INTO `ct_city` VALUES ('203', '鹤壁市', '17');
INSERT INTO `ct_city` VALUES ('204', '新乡市', '17');
INSERT INTO `ct_city` VALUES ('205', '焦作市', '17');
INSERT INTO `ct_city` VALUES ('206', '濮阳市', '17');
INSERT INTO `ct_city` VALUES ('207', '许昌市', '17');
INSERT INTO `ct_city` VALUES ('208', '漯河市', '17');
INSERT INTO `ct_city` VALUES ('209', '三门峡市', '17');
INSERT INTO `ct_city` VALUES ('210', '南阳市', '17');
INSERT INTO `ct_city` VALUES ('211', '商丘市', '17');
INSERT INTO `ct_city` VALUES ('212', '信阳市', '17');
INSERT INTO `ct_city` VALUES ('213', '周口市', '17');
INSERT INTO `ct_city` VALUES ('214', '驻马店市', '17');
INSERT INTO `ct_city` VALUES ('215', '济源市', '17');
INSERT INTO `ct_city` VALUES ('216', '武汉市', '1');
INSERT INTO `ct_city` VALUES ('217', '黄石市', '1');
INSERT INTO `ct_city` VALUES ('218', '十堰市', '1');
INSERT INTO `ct_city` VALUES ('219', '荆州市', '1');
INSERT INTO `ct_city` VALUES ('220', '宜昌市', '1');
INSERT INTO `ct_city` VALUES ('221', '襄阳市', '1');
INSERT INTO `ct_city` VALUES ('222', '鄂州市', '1');
INSERT INTO `ct_city` VALUES ('223', '荆门市', '1');
INSERT INTO `ct_city` VALUES ('224', '孝感市', '1');
INSERT INTO `ct_city` VALUES ('225', '黄冈市', '1');
INSERT INTO `ct_city` VALUES ('226', '咸宁市', '1');
INSERT INTO `ct_city` VALUES ('227', '随州市', '1');
INSERT INTO `ct_city` VALUES ('228', '仙桃市', '1');
INSERT INTO `ct_city` VALUES ('229', '天门市', '1');
INSERT INTO `ct_city` VALUES ('230', '潜江市', '1');
INSERT INTO `ct_city` VALUES ('231', '神农架林区', '1');
INSERT INTO `ct_city` VALUES ('232', '恩施土家族苗族自治州', '1');
INSERT INTO `ct_city` VALUES ('233', '长沙市', '19');
INSERT INTO `ct_city` VALUES ('234', '株洲市', '19');
INSERT INTO `ct_city` VALUES ('235', '湘潭市', '19');
INSERT INTO `ct_city` VALUES ('236', '衡阳市', '19');
INSERT INTO `ct_city` VALUES ('237', '邵阳市', '19');
INSERT INTO `ct_city` VALUES ('238', '岳阳市', '19');
INSERT INTO `ct_city` VALUES ('239', '常德市', '19');
INSERT INTO `ct_city` VALUES ('240', '张家界市', '19');
INSERT INTO `ct_city` VALUES ('241', '益阳市', '19');
INSERT INTO `ct_city` VALUES ('242', '郴州市', '19');
INSERT INTO `ct_city` VALUES ('243', '永州市', '19');
INSERT INTO `ct_city` VALUES ('244', '怀化市', '19');
INSERT INTO `ct_city` VALUES ('245', '娄底市', '19');
INSERT INTO `ct_city` VALUES ('246', '湘西土家族苗族自治州', '19');
INSERT INTO `ct_city` VALUES ('247', '广州市', '20');
INSERT INTO `ct_city` VALUES ('248', '深圳市', '20');
INSERT INTO `ct_city` VALUES ('249', '珠海市', '20');
INSERT INTO `ct_city` VALUES ('250', '汕头市', '20');
INSERT INTO `ct_city` VALUES ('251', '韶关市', '20');
INSERT INTO `ct_city` VALUES ('252', '佛山市', '20');
INSERT INTO `ct_city` VALUES ('253', '江门市', '20');
INSERT INTO `ct_city` VALUES ('254', '湛江市', '20');
INSERT INTO `ct_city` VALUES ('255', '茂名市', '20');
INSERT INTO `ct_city` VALUES ('256', '肇庆市', '20');
INSERT INTO `ct_city` VALUES ('257', '惠州市', '20');
INSERT INTO `ct_city` VALUES ('258', '梅州市', '20');
INSERT INTO `ct_city` VALUES ('259', '汕尾市', '20');
INSERT INTO `ct_city` VALUES ('260', '河源市', '20');
INSERT INTO `ct_city` VALUES ('261', '阳江市', '20');
INSERT INTO `ct_city` VALUES ('262', '清远市', '20');
INSERT INTO `ct_city` VALUES ('263', '东莞市', '20');
INSERT INTO `ct_city` VALUES ('264', '中山市', '20');
INSERT INTO `ct_city` VALUES ('265', '潮州市', '20');
INSERT INTO `ct_city` VALUES ('266', '揭阳市', '20');
INSERT INTO `ct_city` VALUES ('267', '云浮市', '20');
INSERT INTO `ct_city` VALUES ('268', '兰州市', '21');
INSERT INTO `ct_city` VALUES ('269', '金昌市', '21');
INSERT INTO `ct_city` VALUES ('270', '白银市', '21');
INSERT INTO `ct_city` VALUES ('271', '天水市', '21');
INSERT INTO `ct_city` VALUES ('272', '嘉峪关市', '21');
INSERT INTO `ct_city` VALUES ('273', '武威市', '21');
INSERT INTO `ct_city` VALUES ('274', '张掖市', '21');
INSERT INTO `ct_city` VALUES ('275', '平凉市', '21');
INSERT INTO `ct_city` VALUES ('276', '酒泉市', '21');
INSERT INTO `ct_city` VALUES ('277', '庆阳市', '21');
INSERT INTO `ct_city` VALUES ('278', '定西市', '21');
INSERT INTO `ct_city` VALUES ('279', '陇南市', '21');
INSERT INTO `ct_city` VALUES ('280', '临夏回族自治州', '21');
INSERT INTO `ct_city` VALUES ('281', '甘南藏族自治州', '21');
INSERT INTO `ct_city` VALUES ('282', '成都市', '22');
INSERT INTO `ct_city` VALUES ('283', '自贡市', '22');
INSERT INTO `ct_city` VALUES ('284', '攀枝花市', '22');
INSERT INTO `ct_city` VALUES ('285', '泸州市', '22');
INSERT INTO `ct_city` VALUES ('286', '德阳市', '22');
INSERT INTO `ct_city` VALUES ('287', '绵阳市', '22');
INSERT INTO `ct_city` VALUES ('288', '广元市', '22');
INSERT INTO `ct_city` VALUES ('289', '遂宁市', '22');
INSERT INTO `ct_city` VALUES ('290', '内江市', '22');
INSERT INTO `ct_city` VALUES ('291', '乐山市', '22');
INSERT INTO `ct_city` VALUES ('292', '南充市', '22');
INSERT INTO `ct_city` VALUES ('293', '眉山市', '22');
INSERT INTO `ct_city` VALUES ('294', '宜宾市', '22');
INSERT INTO `ct_city` VALUES ('295', '广安市', '22');
INSERT INTO `ct_city` VALUES ('296', '达州市', '22');
INSERT INTO `ct_city` VALUES ('297', '雅安市', '22');
INSERT INTO `ct_city` VALUES ('298', '巴中市', '22');
INSERT INTO `ct_city` VALUES ('299', '资阳市', '22');
INSERT INTO `ct_city` VALUES ('300', '阿坝藏族羌族自治州', '22');
INSERT INTO `ct_city` VALUES ('301', '甘孜藏族自治州', '22');
INSERT INTO `ct_city` VALUES ('302', '凉山彝族自治州', '22');
INSERT INTO `ct_city` VALUES ('303', '贵阳市', '23');
INSERT INTO `ct_city` VALUES ('304', '六盘水市', '23');
INSERT INTO `ct_city` VALUES ('305', '遵义市', '23');
INSERT INTO `ct_city` VALUES ('306', '安顺市', '23');
INSERT INTO `ct_city` VALUES ('307', '铜仁地区', '23');
INSERT INTO `ct_city` VALUES ('308', '毕节地区', '23');
INSERT INTO `ct_city` VALUES ('309', '黔西南布依族苗族自治州', '23');
INSERT INTO `ct_city` VALUES ('310', '黔东南苗族侗族自治州', '23');
INSERT INTO `ct_city` VALUES ('311', '黔南布依族苗族自治州', '23');
INSERT INTO `ct_city` VALUES ('312', '海口市', '24');
INSERT INTO `ct_city` VALUES ('313', '三亚市', '24');
INSERT INTO `ct_city` VALUES ('314', '五指山市', '24');
INSERT INTO `ct_city` VALUES ('315', '琼海市', '24');
INSERT INTO `ct_city` VALUES ('316', '儋州市', '24');
INSERT INTO `ct_city` VALUES ('317', '文昌市', '24');
INSERT INTO `ct_city` VALUES ('318', '万宁市', '24');
INSERT INTO `ct_city` VALUES ('319', '东方市', '24');
INSERT INTO `ct_city` VALUES ('320', '澄迈县', '24');
INSERT INTO `ct_city` VALUES ('321', '定安县', '24');
INSERT INTO `ct_city` VALUES ('322', '屯昌县', '24');
INSERT INTO `ct_city` VALUES ('323', '临高县', '24');
INSERT INTO `ct_city` VALUES ('324', '白沙黎族自治县', '24');
INSERT INTO `ct_city` VALUES ('325', '昌江黎族自治县', '24');
INSERT INTO `ct_city` VALUES ('326', '乐东黎族自治县', '24');
INSERT INTO `ct_city` VALUES ('327', '陵水黎族自治县', '24');
INSERT INTO `ct_city` VALUES ('328', '保亭黎族苗族自治县', '24');
INSERT INTO `ct_city` VALUES ('329', '琼中黎族苗族自治县', '24');
INSERT INTO `ct_city` VALUES ('330', '昆明市', '25');
INSERT INTO `ct_city` VALUES ('331', '曲靖市', '25');
INSERT INTO `ct_city` VALUES ('332', '玉溪市', '25');
INSERT INTO `ct_city` VALUES ('333', '保山市', '25');
INSERT INTO `ct_city` VALUES ('334', '昭通市', '25');
INSERT INTO `ct_city` VALUES ('335', '丽江市', '25');
INSERT INTO `ct_city` VALUES ('336', '思茅市', '25');
INSERT INTO `ct_city` VALUES ('337', '临沧市', '25');
INSERT INTO `ct_city` VALUES ('338', '文山壮族苗族自治州', '25');
INSERT INTO `ct_city` VALUES ('339', '红河哈尼族彝族自治州', '25');
INSERT INTO `ct_city` VALUES ('340', '西双版纳傣族自治州', '25');
INSERT INTO `ct_city` VALUES ('341', '楚雄彝族自治州', '25');
INSERT INTO `ct_city` VALUES ('342', '大理白族自治州', '25');
INSERT INTO `ct_city` VALUES ('343', '德宏傣族景颇族自治州', '25');
INSERT INTO `ct_city` VALUES ('344', '怒江傈傈族自治州', '25');
INSERT INTO `ct_city` VALUES ('345', '迪庆藏族自治州', '25');
INSERT INTO `ct_city` VALUES ('346', '西宁市', '26');
INSERT INTO `ct_city` VALUES ('347', '海东地区', '26');
INSERT INTO `ct_city` VALUES ('348', '海北藏族自治州', '26');
INSERT INTO `ct_city` VALUES ('349', '黄南藏族自治州', '26');
INSERT INTO `ct_city` VALUES ('350', '海南藏族自治州', '26');
INSERT INTO `ct_city` VALUES ('351', '果洛藏族自治州', '26');
INSERT INTO `ct_city` VALUES ('352', '玉树藏族自治州', '26');
INSERT INTO `ct_city` VALUES ('353', '海西蒙古族藏族自治州', '26');
INSERT INTO `ct_city` VALUES ('354', '西安市', '27');
INSERT INTO `ct_city` VALUES ('355', '铜川市', '27');
INSERT INTO `ct_city` VALUES ('356', '宝鸡市', '27');
INSERT INTO `ct_city` VALUES ('357', '咸阳市', '27');
INSERT INTO `ct_city` VALUES ('358', '渭南市', '27');
INSERT INTO `ct_city` VALUES ('359', '延安市', '27');
INSERT INTO `ct_city` VALUES ('360', '汉中市', '27');
INSERT INTO `ct_city` VALUES ('361', '榆林市', '27');
INSERT INTO `ct_city` VALUES ('362', '安康市', '27');
INSERT INTO `ct_city` VALUES ('363', '商洛市', '27');
INSERT INTO `ct_city` VALUES ('364', '南宁市', '28');
INSERT INTO `ct_city` VALUES ('365', '柳州市', '28');
INSERT INTO `ct_city` VALUES ('366', '桂林市', '28');
INSERT INTO `ct_city` VALUES ('367', '梧州市', '28');
INSERT INTO `ct_city` VALUES ('368', '北海市', '28');
INSERT INTO `ct_city` VALUES ('369', '防城港市', '28');
INSERT INTO `ct_city` VALUES ('370', '钦州市', '28');
INSERT INTO `ct_city` VALUES ('371', '贵港市', '28');
INSERT INTO `ct_city` VALUES ('372', '玉林市', '28');
INSERT INTO `ct_city` VALUES ('373', '百色市', '28');
INSERT INTO `ct_city` VALUES ('374', '贺州市', '28');
INSERT INTO `ct_city` VALUES ('375', '河池市', '28');
INSERT INTO `ct_city` VALUES ('376', '来宾市', '28');
INSERT INTO `ct_city` VALUES ('377', '崇左市', '28');
INSERT INTO `ct_city` VALUES ('378', '拉萨市', '29');
INSERT INTO `ct_city` VALUES ('379', '那曲地区', '29');
INSERT INTO `ct_city` VALUES ('380', '昌都地区', '29');
INSERT INTO `ct_city` VALUES ('381', '山南地区', '29');
INSERT INTO `ct_city` VALUES ('382', '日喀则地区', '29');
INSERT INTO `ct_city` VALUES ('383', '阿里地区', '29');
INSERT INTO `ct_city` VALUES ('384', '林芝地区', '29');
INSERT INTO `ct_city` VALUES ('385', '银川市', '30');
INSERT INTO `ct_city` VALUES ('386', '石嘴山市', '30');
INSERT INTO `ct_city` VALUES ('387', '吴忠市', '30');
INSERT INTO `ct_city` VALUES ('388', '固原市', '30');
INSERT INTO `ct_city` VALUES ('389', '中卫市', '30');
INSERT INTO `ct_city` VALUES ('390', '乌鲁木齐市', '31');
INSERT INTO `ct_city` VALUES ('391', '克拉玛依市', '31');
INSERT INTO `ct_city` VALUES ('392', '石河子市　', '31');
INSERT INTO `ct_city` VALUES ('393', '阿拉尔市', '31');
INSERT INTO `ct_city` VALUES ('394', '图木舒克市', '31');
INSERT INTO `ct_city` VALUES ('395', '五家渠市', '31');
INSERT INTO `ct_city` VALUES ('396', '吐鲁番市', '31');
INSERT INTO `ct_city` VALUES ('397', '阿克苏市', '31');
INSERT INTO `ct_city` VALUES ('398', '喀什市', '31');
INSERT INTO `ct_city` VALUES ('399', '哈密市', '31');
INSERT INTO `ct_city` VALUES ('400', '和田市', '31');
INSERT INTO `ct_city` VALUES ('401', '阿图什市', '31');
INSERT INTO `ct_city` VALUES ('402', '库尔勒市', '31');
INSERT INTO `ct_city` VALUES ('403', '昌吉市　', '31');
INSERT INTO `ct_city` VALUES ('404', '阜康市', '31');
INSERT INTO `ct_city` VALUES ('405', '米泉市', '31');
INSERT INTO `ct_city` VALUES ('406', '博乐市', '31');
INSERT INTO `ct_city` VALUES ('407', '伊宁市', '31');
INSERT INTO `ct_city` VALUES ('408', '奎屯市', '31');
INSERT INTO `ct_city` VALUES ('409', '塔城市', '31');
INSERT INTO `ct_city` VALUES ('410', '乌苏市', '31');
INSERT INTO `ct_city` VALUES ('411', '阿勒泰市', '31');
INSERT INTO `ct_city` VALUES ('412', '呼和浩特市', '32');
INSERT INTO `ct_city` VALUES ('413', '包头市', '32');
INSERT INTO `ct_city` VALUES ('414', '乌海市', '32');
INSERT INTO `ct_city` VALUES ('415', '赤峰市', '32');
INSERT INTO `ct_city` VALUES ('416', '通辽市', '32');
INSERT INTO `ct_city` VALUES ('417', '鄂尔多斯市', '32');
INSERT INTO `ct_city` VALUES ('418', '呼伦贝尔市', '32');
INSERT INTO `ct_city` VALUES ('419', '巴彦淖尔市', '32');
INSERT INTO `ct_city` VALUES ('420', '乌兰察布市', '32');
INSERT INTO `ct_city` VALUES ('421', '锡林郭勒盟', '32');
INSERT INTO `ct_city` VALUES ('422', '兴安盟', '32');
INSERT INTO `ct_city` VALUES ('423', '阿拉善盟', '32');
INSERT INTO `ct_city` VALUES ('424', '澳门特别行政区', '33');
INSERT INTO `ct_city` VALUES ('425', '香港特别行政区', '34');

-- ----------------------------
-- Table structure for `ct_comment`
-- ----------------------------
DROP TABLE IF EXISTS `ct_comment`;
CREATE TABLE `ct_comment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tid` int(11) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  `nickname` varchar(255) DEFAULT NULL,
  `header` varchar(255) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `addtime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of ct_comment
-- ----------------------------

-- ----------------------------
-- Table structure for `ct_config`
-- ----------------------------
DROP TABLE IF EXISTS `ct_config`;
CREATE TABLE `ct_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cname` varchar(50) DEFAULT NULL COMMENT '变量title',
  `tkey` varchar(50) DEFAULT NULL COMMENT '变量分组key',
  `key` varchar(50) DEFAULT NULL COMMENT '变量key',
  `val` varchar(250) DEFAULT NULL COMMENT '变量值',
  `bak` varchar(250) DEFAULT NULL COMMENT '提示内容',
  `imports` varchar(20) DEFAULT NULL COMMENT '输入方式 input  radio',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='系统配置表';

-- ----------------------------
-- Records of ct_config
-- ----------------------------

-- ----------------------------
-- Table structure for `ct_industry`
-- ----------------------------
DROP TABLE IF EXISTS `ct_industry`;
CREATE TABLE `ct_industry` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `cname` varchar(20) NOT NULL COMMENT '地区名称',
  `parent_id` int(11) NOT NULL DEFAULT '0' COMMENT '父级id',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=142 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of ct_industry
-- ----------------------------
INSERT INTO `ct_industry` VALUES ('1', 'IT/通信/互联网', '0');
INSERT INTO `ct_industry` VALUES ('2', '经融行业', '0');
INSERT INTO `ct_industry` VALUES ('3', '机械机电/自动化', '0');
INSERT INTO `ct_industry` VALUES ('4', '专业服务', '0');
INSERT INTO `ct_industry` VALUES ('5', '冶金冶炼/五金/采掘', '0');
INSERT INTO `ct_industry` VALUES ('6', '化工行业', '0');
INSERT INTO `ct_industry` VALUES ('7', '纺织服装/皮革鞋帽', '0');
INSERT INTO `ct_industry` VALUES ('8', '电子电器/仪器仪表', '0');
INSERT INTO `ct_industry` VALUES ('9', '快消品/办公用品', '0');
INSERT INTO `ct_industry` VALUES ('10', '房产/建筑/城建/环保', '0');
INSERT INTO `ct_industry` VALUES ('11', '制药/医疗', '0');
INSERT INTO `ct_industry` VALUES ('12', '生活服务/娱乐休闲', '0');
INSERT INTO `ct_industry` VALUES ('13', '交通工具/运输物流', '0');
INSERT INTO `ct_industry` VALUES ('14', '批发/零售/贸易', '0');
INSERT INTO `ct_industry` VALUES ('15', '广告/媒体', '0');
INSERT INTO `ct_industry` VALUES ('16', '教育/科研/培训', '0');
INSERT INTO `ct_industry` VALUES ('17', '造纸/印刷', '0');
INSERT INTO `ct_industry` VALUES ('18', '包装/工艺礼品/奢侈品', '0');
INSERT INTO `ct_industry` VALUES ('19', '能源/资源', '0');
INSERT INTO `ct_industry` VALUES ('20', '农/林/牧/渔', '0');
INSERT INTO `ct_industry` VALUES ('21', '政府/非盈利机构/其他', '0');
INSERT INTO `ct_industry` VALUES ('22', '互联网/电子商务', '1');
INSERT INTO `ct_industry` VALUES ('23', '计算机服务(系统/数据/维护)', '1');
INSERT INTO `ct_industry` VALUES ('24', '计算机软件', '1');
INSERT INTO `ct_industry` VALUES ('25', '计算机硬件', '1');
INSERT INTO `ct_industry` VALUES ('26', '网络游戏', '1');
INSERT INTO `ct_industry` VALUES ('27', '通信/电信/网络设备', '1');
INSERT INTO `ct_industry` VALUES ('28', '通信/电信运营/增值服务', '1');
INSERT INTO `ct_industry` VALUES ('29', '电子技术/半导体/集成电路', '1');
INSERT INTO `ct_industry` VALUES ('30', '其他', '1');
INSERT INTO `ct_industry` VALUES ('31', '基金/证券/期货/投资', '2');
INSERT INTO `ct_industry` VALUES ('32', '银行', '2');
INSERT INTO `ct_industry` VALUES ('33', '保险', '2');
INSERT INTO `ct_industry` VALUES ('34', '信托/担保/拍卖/典当', '2');
INSERT INTO `ct_industry` VALUES ('35', '经融租赁', '2');
INSERT INTO `ct_industry` VALUES ('36', '其他', '2');
INSERT INTO `ct_industry` VALUES ('37', '机床及附件', '3');
INSERT INTO `ct_industry` VALUES ('38', '行业机械设备', '3');
INSERT INTO `ct_industry` VALUES ('39', '专用机械设备', '3');
INSERT INTO `ct_industry` VALUES ('40', '动力电力机电设备', '3');
INSERT INTO `ct_industry` VALUES ('41', '工业自动化', '3');
INSERT INTO `ct_industry` VALUES ('42', '其他', '3');
INSERT INTO `ct_industry` VALUES ('43', '会展及活动服务', '4');
INSERT INTO `ct_industry` VALUES ('44', '财务/审计/税务', '4');
INSERT INTO `ct_industry` VALUES ('45', '管理咨询', '4');
INSERT INTO `ct_industry` VALUES ('46', '人力资源', '4');
INSERT INTO `ct_industry` VALUES ('47', '法律服务', '4');
INSERT INTO `ct_industry` VALUES ('48', '技术服务', '4');
INSERT INTO `ct_industry` VALUES ('49', '检测认证', '4');
INSERT INTO `ct_industry` VALUES ('50', '租赁服务', '4');
INSERT INTO `ct_industry` VALUES ('51', '中介服务', '4');
INSERT INTO `ct_industry` VALUES ('52', '外包服务', '4');
INSERT INTO `ct_industry` VALUES ('53', '翻译服务', '4');
INSERT INTO `ct_industry` VALUES ('54', '其他', '4');
INSERT INTO `ct_industry` VALUES ('55', '冶金冶炼业', '5');
INSERT INTO `ct_industry` VALUES ('56', '五金工具', '5');
INSERT INTO `ct_industry` VALUES ('57', '不锈钢/铝合金/金属制品', '5');
INSERT INTO `ct_industry` VALUES ('58', '采掘业', '5');
INSERT INTO `ct_industry` VALUES ('59', '其他', '5');
INSERT INTO `ct_industry` VALUES ('60', '石油化工', '6');
INSERT INTO `ct_industry` VALUES ('61', '化工原料及产品', '6');
INSERT INTO `ct_industry` VALUES ('62', '涂料/油墨/颜料/燃料', '6');
INSERT INTO `ct_industry` VALUES ('63', '橡胶塑料及制品', '6');
INSERT INTO `ct_industry` VALUES ('64', '玻璃/陶瓷', '6');
INSERT INTO `ct_industry` VALUES ('65', '其他', '6');
INSERT INTO `ct_industry` VALUES ('66', '纺织及成品', '7');
INSERT INTO `ct_industry` VALUES ('67', '印染/染整', '7');
INSERT INTO `ct_industry` VALUES ('68', '服装', '7');
INSERT INTO `ct_industry` VALUES ('69', '皮革/羊毛/羽绒制品', '7');
INSERT INTO `ct_industry` VALUES ('70', '鞋帽', '7');
INSERT INTO `ct_industry` VALUES ('71', '其他', '7');
INSERT INTO `ct_industry` VALUES ('72', '电气设备', '8');
INSERT INTO `ct_industry` VALUES ('73', '电子器材', '8');
INSERT INTO `ct_industry` VALUES ('74', '电工器材', '8');
INSERT INTO `ct_industry` VALUES ('75', '照明工业', '8');
INSERT INTO `ct_industry` VALUES ('76', '数码家电', '8');
INSERT INTO `ct_industry` VALUES ('77', '仪器仪表', '8');
INSERT INTO `ct_industry` VALUES ('78', '其他', '8');
INSERT INTO `ct_industry` VALUES ('79', '快速消耗品', '9');
INSERT INTO `ct_industry` VALUES ('80', '办公用品及设备', '9');
INSERT INTO `ct_industry` VALUES ('81', '其他', '9');
INSERT INTO `ct_industry` VALUES ('82', '房地产', '10');
INSERT INTO `ct_industry` VALUES ('83', '建筑工程', '10');
INSERT INTO `ct_industry` VALUES ('84', '建材装修', '10');
INSERT INTO `ct_industry` VALUES ('85', '城市建设', '10');
INSERT INTO `ct_industry` VALUES ('86', '环境保护', '10');
INSERT INTO `ct_industry` VALUES ('87', '其他', '10');
INSERT INTO `ct_industry` VALUES ('88', '制药/生物工程', '11');
INSERT INTO `ct_industry` VALUES ('89', '医疗/护理/卫生', '11');
INSERT INTO `ct_industry` VALUES ('90', '医疗设备/器械', '11');
INSERT INTO `ct_industry` VALUES ('91', '其他', '11');
INSERT INTO `ct_industry` VALUES ('92', '餐饮业', '12');
INSERT INTO `ct_industry` VALUES ('93', '酒店/旅游', '12');
INSERT INTO `ct_industry` VALUES ('94', '美容/保健', '12');
INSERT INTO `ct_industry` VALUES ('95', '娱乐/休闲/体育', '12');
INSERT INTO `ct_industry` VALUES ('96', '其他', '12');
INSERT INTO `ct_industry` VALUES ('97', '汽车及零配件', '13');
INSERT INTO `ct_industry` VALUES ('98', '船舶及零配件', '13');
INSERT INTO `ct_industry` VALUES ('99', '航空铁路及轨道交通', '13');
INSERT INTO `ct_industry` VALUES ('100', '摩托车及非机动车', '13');
INSERT INTO `ct_industry` VALUES ('101', '交通运输', '13');
INSERT INTO `ct_industry` VALUES ('102', '城市公共交通', '13');
INSERT INTO `ct_industry` VALUES ('103', '物流(邮政)/仓储', '13');
INSERT INTO `ct_industry` VALUES ('104', '其他', '13');
INSERT INTO `ct_industry` VALUES ('105', '批发', '14');
INSERT INTO `ct_industry` VALUES ('106', '零售', '14');
INSERT INTO `ct_industry` VALUES ('107', '贸易/进出口', '14');
INSERT INTO `ct_industry` VALUES ('108', '其他', '14');
INSERT INTO `ct_industry` VALUES ('109', '广告', '15');
INSERT INTO `ct_industry` VALUES ('110', '公关/市场营销', '15');
INSERT INTO `ct_industry` VALUES ('111', '广播/电视媒体', '15');
INSERT INTO `ct_industry` VALUES ('112', '网络新媒体', '15');
INSERT INTO `ct_industry` VALUES ('113', '影视艺术/文化传播', '15');
INSERT INTO `ct_industry` VALUES ('114', '文字媒体/出版', '15');
INSERT INTO `ct_industry` VALUES ('115', '其他', '15');
INSERT INTO `ct_industry` VALUES ('116', '教育/院校', '16');
INSERT INTO `ct_industry` VALUES ('117', '培训', '16');
INSERT INTO `ct_industry` VALUES ('118', '其他', '16');
INSERT INTO `ct_industry` VALUES ('119', '造纸/纸张/纸品', '17');
INSERT INTO `ct_industry` VALUES ('120', '印刷/制版', '17');
INSERT INTO `ct_industry` VALUES ('121', '其他', '17');
INSERT INTO `ct_industry` VALUES ('122', '包装', '18');
INSERT INTO `ct_industry` VALUES ('123', '钟表眼镜', '18');
INSERT INTO `ct_industry` VALUES ('124', '工艺品', '18');
INSERT INTO `ct_industry` VALUES ('125', '礼品', '18');
INSERT INTO `ct_industry` VALUES ('126', '奢侈品/收藏品', '18');
INSERT INTO `ct_industry` VALUES ('127', '其他', '18');
INSERT INTO `ct_industry` VALUES ('128', '常规能源(煤、石油、天然气等)', '19');
INSERT INTO `ct_industry` VALUES ('129', '新能源(风能、太阳能、地热能等)', '19');
INSERT INTO `ct_industry` VALUES ('130', '电力/热力/水利', '19');
INSERT INTO `ct_industry` VALUES ('131', '电池/蓄电池', '19');
INSERT INTO `ct_industry` VALUES ('132', '其他', '19');
INSERT INTO `ct_industry` VALUES ('133', '农产品种植、采集', '20');
INSERT INTO `ct_industry` VALUES ('134', '林木产品种植、采集', '20');
INSERT INTO `ct_industry` VALUES ('135', '水产品养殖、捕捞', '20');
INSERT INTO `ct_industry` VALUES ('136', '农林牧渔相关服务业', '20');
INSERT INTO `ct_industry` VALUES ('137', '牧畜、家禽饲养', '20');
INSERT INTO `ct_industry` VALUES ('138', '其他', '20');
INSERT INTO `ct_industry` VALUES ('139', '政府/公共事业', '21');
INSERT INTO `ct_industry` VALUES ('140', '非盈利机构', '21');
INSERT INTO `ct_industry` VALUES ('141', '其他', '21');

-- ----------------------------
-- Table structure for `ct_shop`
-- ----------------------------
DROP TABLE IF EXISTS `ct_shop`;
CREATE TABLE `ct_shop` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `header` varchar(255) DEFAULT NULL,
  `sname` varchar(50) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `latitude` varchar(100) DEFAULT NULL,
  `longitude` varchar(100) DEFAULT NULL,
  `wifi` int(2) DEFAULT '0',
  `stopc` int(2) DEFAULT '0',
  `alipay` int(2) DEFAULT '0',
  `wxpay` int(2) DEFAULT NULL,
  `star_time` varchar(255) DEFAULT NULL,
  `content` varchar(500) DEFAULT NULL,
  `thumb` varchar(1000) DEFAULT NULL,
  `scid` int(11) DEFAULT NULL,
  `scname` varchar(255) DEFAULT NULL,
  `cid` int(11) DEFAULT NULL,
  `cname` varchar(255) DEFAULT NULL,
  `sort` int(11) DEFAULT '0',
  `hits` int(11) DEFAULT '0',
  `uv` int(11) DEFAULT '0',
  `sc` int(11) DEFAULT '0',
  `addtime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid` (`uid`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of ct_shop
-- ----------------------------

-- ----------------------------
-- Table structure for `ct_shop_cat`
-- ----------------------------
DROP TABLE IF EXISTS `ct_shop_cat`;
CREATE TABLE `ct_shop_cat` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cname` varchar(100) NOT NULL COMMENT '分类名称',
  `icon` varchar(255) DEFAULT NULL,
  `sort` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ct_shop_cat
-- ----------------------------
INSERT INTO `ct_shop_cat` VALUES ('1', '电器', null, '4');
INSERT INTO `ct_shop_cat` VALUES ('2', '电脑', null, '7');
INSERT INTO `ct_shop_cat` VALUES ('4', '服装', null, '5');
INSERT INTO `ct_shop_cat` VALUES ('5', '苹果笔记本', null, '1');
INSERT INTO `ct_shop_cat` VALUES ('6', '小米手机', null, '8');
INSERT INTO `ct_shop_cat` VALUES ('11', '分类五', null, '9');
INSERT INTO `ct_shop_cat` VALUES ('9', '手机', null, '8');
INSERT INTO `ct_shop_cat` VALUES ('12', '分类六', null, '10');
INSERT INTO `ct_shop_cat` VALUES ('13', '分类七', null, '11');

-- ----------------------------
-- Table structure for `ct_shop_sc`
-- ----------------------------
DROP TABLE IF EXISTS `ct_shop_sc`;
CREATE TABLE `ct_shop_sc` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `sid` int(11) DEFAULT NULL,
  `sname` varchar(255) DEFAULT NULL,
  `header` varchar(255) DEFAULT NULL,
  `content` varchar(50) DEFAULT NULL,
  `addtime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of ct_shop_sc
-- ----------------------------

-- ----------------------------
-- Table structure for `ct_shop_uv`
-- ----------------------------
DROP TABLE IF EXISTS `ct_shop_uv`;
CREATE TABLE `ct_shop_uv` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `sid` int(11) DEFAULT NULL,
  `num` int(11) DEFAULT '0',
  `addtime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of ct_shop_uv
-- ----------------------------

-- ----------------------------
-- Table structure for `ct_tie`
-- ----------------------------
DROP TABLE IF EXISTS `ct_tie`;
CREATE TABLE `ct_tie` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `lid` int(11) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  `header` varchar(255) DEFAULT NULL,
  `nickname` varchar(255) DEFAULT NULL,
  `cid` int(11) DEFAULT NULL,
  `cname` varchar(255) DEFAULT NULL,
  `content` varchar(500) DEFAULT NULL,
  `thumb` varchar(600) DEFAULT NULL,
  `zan` varchar(600) DEFAULT NULL,
  `mobile` varchar(13) DEFAULT NULL,
  `comment` varchar(600) DEFAULT NULL,
  `address_name` varchar(255) DEFAULT NULL,
  `latitude` varchar(255) DEFAULT NULL,
  `longitude` varchar(255) DEFAULT NULL,
  `addtime` int(11) DEFAULT NULL,
  `hits` int(11) DEFAULT '0',
  `state` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of ct_tie
-- ----------------------------

-- ----------------------------
-- Table structure for `ct_tie_cat`
-- ----------------------------
DROP TABLE IF EXISTS `ct_tie_cat`;
CREATE TABLE `ct_tie_cat` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cname` varchar(100) NOT NULL COMMENT '分类名称',
  `icon` varchar(255) DEFAULT NULL,
  `sort` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ct_tie_cat
-- ----------------------------
INSERT INTO `ct_tie_cat` VALUES ('1', '电器', null, '4');
INSERT INTO `ct_tie_cat` VALUES ('2', '电脑', null, '7');
INSERT INTO `ct_tie_cat` VALUES ('4', '服装', null, '5');
INSERT INTO `ct_tie_cat` VALUES ('5', '苹果笔记本', null, '1');
INSERT INTO `ct_tie_cat` VALUES ('6', '小米手机', null, '8');
INSERT INTO `ct_tie_cat` VALUES ('11', '分类五', null, '9');
INSERT INTO `ct_tie_cat` VALUES ('9', '手机', null, '8');
INSERT INTO `ct_tie_cat` VALUES ('12', '分类六', null, '10');
INSERT INTO `ct_tie_cat` VALUES ('13', '分类七', null, '11');

-- ----------------------------
-- Table structure for `ct_times`
-- ----------------------------
DROP TABLE IF EXISTS `ct_times`;
CREATE TABLE `ct_times` (
  `times_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(45) DEFAULT NULL,
  `login_ip` char(15) DEFAULT NULL COMMENT 'ip',
  `login_time` int(10) unsigned DEFAULT NULL,
  `group_id` int(10) unsigned DEFAULT NULL,
  `failure_times` int(10) unsigned DEFAULT NULL,
  `is_admin` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`times_id`)
) ENGINE=MyISAM AUTO_INCREMENT=81 DEFAULT CHARSET=utf8 COMMENT='登录错误次数';

-- ----------------------------
-- Records of ct_times
-- ----------------------------

-- ----------------------------
-- Table structure for `ct_user`
-- ----------------------------
DROP TABLE IF EXISTS `ct_user`;
CREATE TABLE `ct_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `openid` varchar(50) DEFAULT NULL COMMENT '微信openid',
  `username` varchar(20) DEFAULT NULL COMMENT '用户名',
  `nickname` varchar(255) DEFAULT NULL COMMENT '用户昵称',
  `nicknames` varchar(100) DEFAULT NULL,
  `thumb` varchar(255) DEFAULT NULL COMMENT '头像',
  `sex` int(1) DEFAULT '0' COMMENT '性别默认女',
  `mobile` varchar(11) DEFAULT '0' COMMENT '手机',
  `integral` int(11) DEFAULT '0',
  `addtime` int(11) DEFAULT '0' COMMENT '注册时间',
  `work` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `p_1` int(11) DEFAULT NULL,
  `p_2` int(11) DEFAULT NULL,
  `p_3` int(11) DEFAULT NULL,
  `gid` int(2) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ct_user
-- ----------------------------
INSERT INTO `ct_user` VALUES ('1', '', 'chaituan', 'btem_QWxsZW4=', 'Allen', 'https://wx.qlogo.cn/mmopen/vi_32/JS8ickiciacJquyYJZ7ulUnIK2IC83TLUFylSEMf4mwFawv44PgqZm8abhD8r3MMcqA449zUCjCQMsX28un700o8A/0', '1', '13538436848', '-35', '1524068425', 'IT行业', '东莞市', null, null, null, '2');
INSERT INTO `ct_user` VALUES ('2', '', null, 'btem_5bCP6Zuo', null, 'https://wx.qlogo.cn/mmopen/vi_32/jMbL0CpEqhvXibKnNo1eHsoeHuXQOfaGCds0WmiakB4gs17kPntPyxBVFt6G6naS0XZLnZPxTrL9wsYgl6iaAiasgw/0', '2', '0', '40', '1524195464', null, null, '1', null, null, '1');
INSERT INTO `ct_user` VALUES ('3', '', null, 'btem_5aSq6Ziz6Iqx', null, 'https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIXY9EZ88aADIqKeJdWXKVWsteh5z5PVANayBPLCsfniaYoibRXVVdEsM0vSGS1k4SuHOic7wCBrrQibw/0', '2', '0', '0', '1524456740', null, null, null, null, null, null);
INSERT INTO `ct_user` VALUES ('4', '', null, 'btem_5ZOI5ZOI', null, 'https://wx.qlogo.cn/mmopen/vi_32/G2hKWVwthOHhS26SicicAAWxiaD93rI7rRE09p4gsbtn6ATwZn8ia49dmIPa3WGzCb1s0xAib0mTDgWBrqIERWurKfg/0', '0', '0', '0', '1524475729', null, null, null, null, null, null);

-- ----------------------------
-- Table structure for `ct_zan`
-- ----------------------------
DROP TABLE IF EXISTS `ct_zan`;
CREATE TABLE `ct_zan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tid` int(11) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  `nickname` varchar(255) DEFAULT NULL,
  `addtime` int(11) DEFAULT NULL,
  `header` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of ct_zan
-- ----------------------------
