const app = getApp();
Page({
  data: {
    mainActiveIndex:0
  },
  onLoad: function (options) {
    if(options.show){
      this.setData({allshow:true})
    }
    this.get_lists();
  },
  get_lists:function(){
    wx.request({
      url: app.globalData.host + 'wechat/api/city',
      method: 'POST',
      header: { 'Content-Type': 'application/x-www-form-urlencoded', 'X-Requested-With': 'XMLHttpRequest' },
      success: d => {
        this.setData({items:d.data.data})
      }
    })
  },
  onClickNav:function(e) {
    this.setData({mainActiveIndex: e.detail.index || 0 });
  },
  onClickItem:function(e) {
    this.setData({ activeId: e.detail.id   });
    //点击返回上一页
    var pages = getCurrentPages();
    var prevpage = pages[pages.length - 2];
    prevpage.setData({ city: { id: e.detail.id, text: e.detail.text } });
    wx.navigateBack({})
  },
  pageScrollToBottom: function () {
    wx.createSelectorQuery().select('#j_page').boundingClientRect( (rect) =>{
      this.setData({ maxHeight: this.data.allshow ? rect.height - 44 : rect.height});
    }).exec()
  },
  onReady: function () {
    this.pageScrollToBottom();
  },
  onAll:function(){
    var pages = getCurrentPages();
    var prevpage = pages[pages.length - 2];
    prevpage.setData({ city: { id: 0, text: '所有区域' } });
    wx.navigateBack({})
  },
  onShow: function () {

  },
  onHide: function () {

  },
  onUnload: function () {

  },
  onPullDownRefresh: function () {

  }
})