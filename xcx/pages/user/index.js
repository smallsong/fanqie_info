const app = getApp();
Page({

  data: {
    tabbar : 1
  },
  onLoad: function (options) {
    if (wx.getStorageSync('userinfo')) {
      this.setData({ user: wx.getStorageSync('userinfo'), uid: parseInt(wx.getStorageSync('FXID')) });
    } else {
      app.userInfoReadyCallback = res => {
        this.setData({ user: res.userInfo,uid: parseInt(wx.getStorageSync('FXID'))});        
      }
    }  
    this.pageScrollToBottom(); 
  },
  onReady: function () {
    
  },
  onShow: function () {
    app.reg();//强制登录
  },
  onPinlun: function () {
    this.setData({ showpopup: true, plinput: true });
  },
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  pageScrollToBottom: function () {
    wx.createSelectorQuery().select('#j_page').boundingClientRect((rect) => {
      var c = rect.width / rect.height;
      this.setData({ x: rect.right - 80, y: rect.bottom - 110 })
    }).exec()
  },
  onShareAppMessage: function () {

  }
})