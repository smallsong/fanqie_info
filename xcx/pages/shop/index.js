const app = getApp();
var cache = require('../../utils/cache.js');
Page({

  data: {
    tabbar: 1,
    state: 0,
    fy: { page: 1, count: 0, end: 1 }//分页
  },
  onLoad: function (options) {
    this.get_ad();
    this.get_cat();    
    this.get_lists();
  },
  onReady: function () {

  },
  onShow: function () {
    
  },
  get_cat: function () {
    wx.request({
      url: app.globalData.host + 'wechat/shop_api/shop_cat',
      method: 'POST',
      header: { 'Content-Type': 'application/x-www-form-urlencoded', 'X-Requested-With': 'XMLHttpRequest' },
      success: d => {
        this.setData({ nav: d.data.data });
      }
    })
  },
  get_ad: function () {
    wx.request({
      url: app.globalData.host + 'wechat/api/ad',
      data: { gid: 2 },
      method: 'POST',
      header: { 'Content-Type': 'application/x-www-form-urlencoded', 'X-Requested-With': 'XMLHttpRequest' },
      success: d => {
        if (d.data.state == 1) {
          this.setData({ ad: d.data.data });
        } else {
          this.setData({ ad: '' });
        }
      }
    });
  },
  get_lists: function () {
    if (!this.data.fy.end) return false;
    wx.showLoading({ title: '玩命加载中', mask: true });
    var data = {};
    if (this.data.state == 1) {
      data = { state: this.data.state, la: this.data.location.la, lg: this.data.location.lg, page: this.data.fy.page, total: this.data.fy.count };
    } else {
      data = { state: this.data.state, page: this.data.fy.page, total: this.data.fy.count };
    }
    wx.request({
      url: app.globalData.host + 'wechat/shop_api/shop_lists',
      method: 'POST',
      data: data,
      header: { 'Content-Type': 'application/x-www-form-urlencoded', 'X-Requested-With': 'XMLHttpRequest' },
      success: d => {
        if (d.data.state == 1) {
          this.data.fy = d.data.mark;
          this.setData({ shop: this.data.shop ? this.data.shop.concat(d.data.data) : d.data.data });
        } else {
          this.data.fy.end = 0;
          this.setData({ shop: '', msg: d.data.message });
        } 
        wx.hideLoading();
      }
    });
  },
  onTab: function (e) {
    var index = e.detail.index;
    this.setData({ state: index });
    this.resetFy();
    if (index == 2) {//获取附近的信息/
      var location = cache.get('location');
      if (location) {//本地缓存的地址信息    
        this.setData({ location });
        this.get_lists();
      } else {//获取最新的地址，并执行获取数据
        this.get_location();
      }
    } else {
      this.get_lists();
    }
  },  
  get_location() {
    wx.showLoading({
      title: '位置计算中',
    })
    wx.getLocation({
      success: res => {//正常获取    
        console.log(res)
        var la = res.latitude, lg = res.longitude;
        cache.put('location', { la: la, lg: lg }, 43200);//存入缓存 过期时间1天
        this.setData({ location: { la: la, lg: lg } });
        this.get_lists();
      },
      fail: res => {//用户拒绝，则提示重新获取
        this.setData({ dialogshow: true });
      },
      complete: function () {
        wx.hideLoading();
      }
    });
  },
  onYx: function () {
    wx.openSetting({
      success: res2 => {
        if (res2.authSetting["scope.userLocation"]) {//如果用户重新同意了授权登录
          wx.showToast({ title: '授权成功', duration: 2000 });
          setTimeout(() => {  //要等2秒 才能收到数据
            this.get_location();
          }, 2000);
        }
      },
      fail: f => {
        console.log(f)
      }
    })
  },
  onYxno: function () {
    this.data.fy.end = 0;
    this.setData({ shop: '', msg: '无法使用此功能!~~~~(>_<)~~~~' });
  },
  adclick:function(e){
    wx.navigateTo({
      url: e.currentTarget.dataset.url
    })
  },
  onHide: function () {

  },
  onUnload: function () {

  },
  onPullDownRefresh: function () {
    this.resetFy();
    this.get_lists();
    wx.stopPullDownRefresh();
  },
  onReachBottom: function () {
    this.get_lists();
  },
  resetFy: function () {//重置分页
    this.data.fy = { page: 1, count: 0, end: 1 };
    this.setData({ shop: '' });
  },
  onSearch: function () {
    wx.navigateTo({
      url: '/pages/search/index?id=2',
    })
  },
  navclick: function (e) {
    var cid = e.currentTarget.id;
    wx.navigateTo({
      url: '/pages/shop/lists/index?cid=' + cid,
    })
  },
  onShareAppMessage: function () {

  }
})